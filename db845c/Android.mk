ifneq ($(filter db845c pixel3_mainline, $(TARGET_BOARD_PLATFORM)),)

LOCAL_PATH := $(call my-dir)

# If some modules are built directly from this directory (not subdirectories),
# their rules should be written here.

include $(call all-makefiles-under,$(LOCAL_PATH))
endif
