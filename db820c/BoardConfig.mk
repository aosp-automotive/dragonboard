include device/linaro/dragonboard/BoardConfigCommon.mk

TARGET_CPU_VARIANT := kryo
TARGET_ARCH_VARIANT := armv8-a
TARGET_2ND_CPU_VARIANT := kryo
TARGET_2ND_ARCH_VARIANT := armv8-a

# Board Information
TARGET_BOOTLOADER_BOARD_NAME := db820c
TARGET_BOARD_PLATFORM := db820c

# Image Configuration
BOARD_KERNEL_BASE := 0x80000000
BOARD_KERNEL_PAGESIZE := 4096
BOARD_KERNEL_CMDLINE := earlycon firmware_class.path=/vendor/firmware/ androidboot.hardware=db820c
BOARD_KERNEL_CMDLINE += overlay_mgr.overlay_dt_entry=hardware_cfg_enable_android_fstab
BOARD_KERNEL_CMDLINE += printk.devkmsg=on deferred_probe_timeout=30
#BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 4294967296 #4G
BOARD_USERDATAIMAGE_PARTITION_SIZE := 12884901888 #12G
TARGET_COPY_OUT_VENDOR := vendor
BOARD_VENDORIMAGE_PARTITION_SIZE := 2147483648 #2G
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDOR_KERNEL_MODULES := \
    $(wildcard device/linaro/dragonboard-kernels/db820c-5.4-modules/*.ko)
