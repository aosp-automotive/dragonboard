$(call inherit-product, device/linaro/dragonboard/db820c.mk)
PRODUCT_PACKAGE_OVERLAYS += device/linaro/dragonboard/automotive/overlay
$(call inherit-product, device/generic/car/common/car.mk)

DEVICE_MANIFEST_FILE += device/linaro/dragonboard/automotive/manifest.xml

# Automotive Audio HAL
include device/linaro/dragonboard/automotive/audio/car_audio.mk

# ADB over IP
PRODUCT_PROPERTY_OVERRIDES += persist.adb.tcp.port=5555

# ueventd.automotive.rc
PRODUCT_COPY_FILES += \
    device/linaro/dragonboard/automotive/ueventd.automotive.rc:$(TARGET_COPY_OUT_ROOT)/ueventd.db820c.rc

# Broadcast Radio HAL
PRODUCT_PACKAGES += android.hardware.broadcastradio@1.1-service.tef6686
PRODUCT_COPY_FILES += \
    device/linaro/dragonboard/automotive/broadcastradio/android.hardware.broadcastradio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.broadcastradio.xml

# Audio debugging packages
PRODUCT_PACKAGES += \
    tinymix \
    tinypcminfo \
    tinyhostless \
    tinycap

# Custom
PRODUCT_PACKAGES += CarConfig \
    thermalfand \
    camd \
    bossac

# Lights
PRODUCT_PACKAGES += \
    lights.db820c \
    android.hardware.light@2.0 \
    android.hardware.light@2.0-service.db820c \
    android.hardware.light@2.0-impl

BOARD_SEPOLICY_DIRS += device/linaro/dragonboard/automotive/sepolicy

# GNSS HAL - GPSD
PRODUCT_PACKAGES += android.hardware.gnss@1.1 \
        android.hardware.gnss@1.1-impl \
        android.hardware.gnss@1.1-service.gpsd \
        gpsd
PRODUCT_PROPERTY_OVERRIDES += \
        service.gpsd.automotive=true \
        service.gpsd.parameters=-N,-D2,/dev/ttyACM0,/dev/ttyACM1
BOARD_SEPOLICY_DIRS += external/gpsd/android/sepolicy

# EVS
#PRODUCT_PACKAGES += \
#	evs_app \
#	evs_app_default_resources \
#	android.automotive.evs.manager@1.0 \
#	android.hardware.automotive.evs@1.0-sample
#BOARD_SEPOLICY_DIRS += packages/services/Car/evs/sepolicy
#PRODUCT_COPY_FILES += device/linaro/dragonboard/automotive/evs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/vintf/manifest/evs.xml

# Prebuilt
include device/linaro/dragonboard/automotive/prebuilt/prebuilt.mk

PRODUCT_NAME := db820c_car
PRODUCT_DEVICE := db820c
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP CAR on db820c
