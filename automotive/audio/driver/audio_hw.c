/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "audio_hw_automotive"

#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <dirent.h>

#include <log/log.h>
#include <cutils/str_parms.h>
#include <cutils/hashmap.h>

#include <hardware/audio.h>
#include <hardware/hardware.h>
#include <system/audio.h>
#include <tinyalsa/asoundlib.h>

#include <audio_utils/channels.h>
#include <audio_utils/resampler.h>

#include "webrtc_wrapper.h"

#define PLAYBACK_DEVICE				0
#define RECORD_DEVICE				1
#define SCO_PLAYBACK_DEVICE			2
#define SCO_RECORD_DEVICE			3

#define OUT_PERIOD_MS				40
#define OUT_PERIOD_COUNT			4
#define IN_PERIOD_MS				40
#define IN_PERIOD_COUNT				4

#define AUDIO_PARAMETER_HFP_ENABLE		"hfp_enable"
#define AUDIO_PARAMETER_HFP_SET_SAMPLING_RATE	"hfp_set_sampling_rate"

// rate for local sound card during HFP
#define HFP_LOCAL_RATE				16000

/* Qualcomm Dragonboard 820c:
 *
 * Local speakers are connected to	QUAT_MI2S_RX
 * Local microphone is connected to	QUAT_MI2S_TX
 * Bluetooth output is connected to	PRI_PCM_RX
 * Bluetooth input is connected to 	PRI_PCM_TX
 *
 * The first 2 frontends will be reserved for playback and capture;
 * tinymix "QUAT_MI2S_RX Audio Mixer MultiMedia1" 1
 * tinymix "MultiMedia2 Mixer QUAT_MI2S_TX" 1
 *
 * The next 2 frontends will be reserved for bluetooth SCO
 * tinymix "PRI_PCM_RX Audio Mixer MultiMedia3" 1
 * tinymix "MultiMedia4 Mixer PRI_PCM_TX" 1
 */

// Mixers
#define MIXER_FRONT_UNMUTE			"DAC-F Digital Playback Switch"
#define MIXER_REAR_UNMUTE			"DAC-R Digital Playback Switch"
#define MIXER_FRONT_GAIN			"DAC-F Digital Playback Volume"
#define MIXER_REAR_GAIN				"DAC-R Digital Playback Volume"
#define MIXER_MIC_MUTE				"Mic Mute"
#define MIXER_MIC_GAIN				"ADC2 Capture Volume"
#define MIXER_MIC_GAIN_DEFAULT			1.0
#define MIXER_RADIO_SOURCE			"Line Playback Source"
#define MIXER_RADIO_GAIN			"Line Playback Volume"
#define MIXER_RADIO_GAIN_DEFAULT		255
#define MIXER_RADIO_EN				2
#define MIXER_RADIO_DIS				0

#define MIXER_MM1				"QUAT_MI2S_RX Audio Mixer MultiMedia1"
#define MIXER_MM2				"MultiMedia2 Mixer QUAT_MI2S_TX"
#define MIXER_MM3				"PRI_PCM_RX Audio Mixer MultiMedia3"
#define MIXER_MM4				"MultiMedia4 Mixer PRI_PCM_TX"

#define _bool_str(x) ((x)?"true":"false")

static pthread_t hfp_thread = 0;
static bool term_hfp = false;
static bool on_phone = false;
static int media_gain;
static int hfp_gain;
static struct pcm *pcm = NULL;
static int CARD = 0;

struct generic_audio_device {
  struct audio_hw_device device;  // Constant after init
  pthread_mutex_t lock;
  unsigned int last_patch_id;   // Protected by this->lock
  bool master_mute;             // Protected by this->lock
  bool mic_mute;                // Protected by this->lock
  struct mixer *mixer;          // Protected by this->lock
  Hashmap *out_bus_stream_map;  // Extended field. Constant after init
};

struct generic_stream_out {
  struct audio_stream_out stream;  // Constant after init
  pthread_mutex_t lock;
  struct generic_audio_device *dev;  // Constant after init
  audio_devices_t device;            // Protected by this->lock
  struct audio_config req_config;    // Constant after init
  struct pcm_config pcm_config;      // Constant after init
  const char *bus_address;           // Extended field. Constant after init
  struct audio_gain gain_stage;      // Constant after init
  float amplitude_ratio;             // Protected by this->lock
  bool standby;                    // Protected by this->lock
};

struct generic_stream_in {
  struct audio_stream_in stream;  // Constant after init
  pthread_mutex_t lock;
  struct generic_audio_device *dev;  // Constant after init
  audio_devices_t device;            // Protected by this->lock
  struct audio_config req_config;    // Constant after init
  struct pcm *pcm;                   // Protected by this->lock
  struct pcm_config pcm_config;      // Constant after init
  int16_t *stereo_to_mono_buf;       // Protected by this->lock
  size_t stereo_to_mono_buf_size;    // Protected by this->lock
  const char *bus_address;           // Extended field. Constant after init

  // Time & Position Keeping
  bool standby;                       // Protected by this->lock
  int64_t standby_position;           // Protected by this->lock
  struct timespec standby_exit_time;  // Protected by this->lock
  int64_t standby_frames_read;        // Protected by this->lock
};

static int adev_get_mic_mute(const struct audio_hw_device *dev, bool *state);
static int adev_open_input_stream(struct audio_hw_device *dev,
        audio_io_handle_t handle, audio_devices_t devices, struct audio_config *config,
        struct audio_stream_in **stream_in, audio_input_flags_t flags __unused, const char *address,
        audio_source_t source __unused);
static void adev_close_input_stream(struct audio_hw_device *dev,
        struct audio_stream_in *stream);

static struct pcm_config pcm_config_out = {
    .channels = 2,
    .rate = 48000,
    .period_size = 1024,
    .period_count = OUT_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
    .start_threshold = 0,
    .stop_threshold = 0,
    .silence_threshold = 0,
};

static struct pcm_config pcm_config_in = {
    .channels = 2,
    .rate = 48000,
    .period_size = 1024,
    .period_count = IN_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
    .start_threshold = 0,
    .stop_threshold = 0,
    .silence_threshold = 0,
};

static pthread_mutex_t adev_init_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t hfp_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t pcm_lock = PTHREAD_MUTEX_INITIALIZER;
static unsigned int audio_device_ref_count = 0;

static uint32_t out_get_sample_rate(const struct audio_stream *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    return out->req_config.sample_rate;
}

static int out_set_sample_rate(struct audio_stream *stream, uint32_t rate) {
    return -ENOSYS;
}

static size_t out_get_buffer_size(const struct audio_stream *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    int size = out->pcm_config.period_size *
                audio_stream_out_frame_size(&out->stream);

    return size;
}

static audio_channel_mask_t out_get_channels(const struct audio_stream *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    return out->req_config.channel_mask;
}

static audio_format_t out_get_format(const struct audio_stream *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    return out->req_config.format;
}

static int out_set_format(struct audio_stream *stream, audio_format_t format) {
    return -ENOSYS;
}

static int out_dump(const struct audio_stream *stream, int fd) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    pthread_mutex_lock(&out->lock);
    dprintf(fd, "\tout_dump:\n"
                "\t\taddress: %s\n"
                "\t\tsample rate: %u\n"
                "\t\tbuffer size: %zu\n"
                "\t\tchannel mask: %08x\n"
                "\t\tformat: %d\n"
                "\t\tdevice: %08x\n"
                "\t\tamplitude ratio: %f\n"
                "\t\taudio dev: %p\n\n",
                out->bus_address,
                out_get_sample_rate(stream),
                out_get_buffer_size(stream),
                out_get_channels(stream),
                out_get_format(stream),
                out->device,
                out->amplitude_ratio,
                out->dev);
    pthread_mutex_unlock(&out->lock);
    return 0;
}

static int out_set_parameters(struct audio_stream *stream, const char *kvpairs) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    struct str_parms *parms;
    char value[32];
    int ret;
    long val;
    char *end;

    pthread_mutex_lock(&out->lock);
    if (!out->standby) {
        //Do not support changing params while stream running
        ret = -ENOSYS;
    } else {
        parms = str_parms_create_str(kvpairs);
        ret = str_parms_get_str(parms, AUDIO_PARAMETER_STREAM_ROUTING,
                                value, sizeof(value));
        if (ret >= 0) {
            errno = 0;
            val = strtol(value, &end, 10);
            if (errno == 0 && (end != NULL) && (*end == '\0') && ((int)val == val)) {
                out->device = (int)val;
                ret = 0;
            } else {
                ret = -EINVAL;
            }
        }
        str_parms_destroy(parms);
    }
    pthread_mutex_unlock(&out->lock);
    return ret;
}

static char *out_get_parameters(const struct audio_stream *stream, const char *keys) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    struct str_parms *query = str_parms_create_str(keys);
    char *str;
    char value[256];
    struct str_parms *reply = str_parms_create();
    int ret;

    ret = str_parms_get_str(query, AUDIO_PARAMETER_STREAM_ROUTING, value, sizeof(value));
    if (ret >= 0) {
        pthread_mutex_lock(&out->lock);
        str_parms_add_int(reply, AUDIO_PARAMETER_STREAM_ROUTING, out->device);
        pthread_mutex_unlock(&out->lock);
        str = strdup(str_parms_to_str(reply));
    } else {
        str = strdup(keys);
    }

    str_parms_destroy(query);
    str_parms_destroy(reply);
    return str;
}

static uint32_t out_get_latency(const struct audio_stream_out *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    return (out->pcm_config.period_size * 1000) / out->pcm_config.rate;
}

static int out_set_volume(struct audio_stream_out *stream,
        float left, float right) {
    return -ENOSYS;
}

// Applies gain naively, assume AUDIO_FORMAT_PCM_16_BIT
// TODO While the streams are given individual gains, we only actually have one output
// line to the sound card. In order to support RADIO volume control, the output hardware gain
// must be set to the gain for bus0_media_out. This ends up being a hard limit for the gain
// of all other streams, which end up all at 0 dB.
/*static void out_apply_gain(struct generic_stream_out *out, const void *buffer, size_t bytes) {
    int16_t *int16_buffer = (int16_t *)buffer;
    size_t int16_size = bytes / sizeof(int16_t);
    for (int i = 0; i < int16_size; i++) {
         float multiplied = int16_buffer[i] * out->amplitude_ratio;
         if (multiplied > INT16_MAX) int16_buffer[i] = INT16_MAX;
         else if (multiplied < INT16_MIN) int16_buffer[i] = INT16_MIN;
         else int16_buffer[i] = (int16_t)multiplied;
    }
}*/

// Find if this stream is the highest priority active stream.
static int out_max_priority(struct generic_stream_out *stream){
    // We have a priority-ordered list of streams. To determine if THIS stream is
    // the active stream of highest priority, we first find the index of this
    // stream in the list. We then try getting each of the higher priority
    // streams from the hashmap, and if we find a higher priority stream which is
    // not in standby, we return 0, meaning that this stream is not high enough
    // priority to playback.

    const char *streams[] = {
        "bus0_media_out",
	"bus1_navigation_out",
	"bus2_voice_command_out",
	"bus3_call_ring_out",
	"bus4_call_out",
	"bus5_alarm_out",
	"bus6_notification_out",
	"bus7_system_sound_out",
	"other"
    };
    int i;
    for (i = 0; i <= 8; i++){
        if (strcmp(stream->bus_address, streams[i]) == 0) break;
    }
    if (i == 8) return 0; // unknown bus must not write.
    for (i = i + 1; i <= 7; i++){
        struct generic_stream_out *mapped_stream = hashmapGet(stream->dev->out_bus_stream_map, (void *)streams[i]);
	if (mapped_stream && mapped_stream->standby == 0) return 0;
    }

    ALOGV("%s: This stream (%s) is max priority, write data to alsa.", __func__, stream->bus_address);
    return 1;
}

static ssize_t out_write(struct audio_stream_out *stream, const void *buffer, size_t bytes) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    const size_t frames =  bytes / audio_stream_out_frame_size(stream);

    pthread_mutex_lock(&out->lock);

    if (out->standby) {
        out->standby = false;
    }

    size_t frames_written = frames;
    if (out->dev->master_mute) {
        ALOGV("%s: ignored due to master mute", __func__);
    } else if (out_max_priority(out)){
        //TODO out_apply_gain(out, buffer, bytes);
        // Note: out_apply_gain can now use the hardware mixer.
        pthread_mutex_lock(&pcm_lock);
	pthread_mutex_lock(&hfp_lock);
        // on_phone is set true before the hfp_thread is started, and thus before the
	// hfp_thread closes pcm. Since we are protected by pcm_lock at this point,
	// and because hfp_thread respects pcm_lock, it is safe to open pcm as long
	// as on_phone is false, and it is safe to write to pcm as long as it has not
	// already been closed by hfp_thread.
        if (!on_phone && pcm == NULL) {
            pthread_mutex_unlock(&hfp_lock);
            pcm = pcm_open(CARD, PLAYBACK_DEVICE,
                    PCM_OUT | PCM_MONOTONIC, &out->pcm_config);
            if (!pcm_is_ready(pcm)) {
                ALOGE("pcm_open(out) failed: %s: address %s channels %d format %d rate %d",
                        pcm_get_error(pcm),
                        out->bus_address,
                        out->pcm_config.channels,
                        out->pcm_config.format,
                        out->pcm_config.rate);
                pcm_close(pcm);
                pcm = NULL;
            }
        } else pthread_mutex_unlock(&hfp_lock);
        if (pcm != NULL){
            int ret;
            ret = pcm_write(pcm, buffer, bytes);
            if (ret != 0)
                ALOGE("%s: pcm_write error: %d", __func__, ret);
        }
        pthread_mutex_unlock(&pcm_lock);
    } else {
        ALOGV("%s: write preempted by higher priority stream.", __func__);
    }

    pthread_mutex_unlock(&out->lock);

    if (frames_written < frames) {
        ALOGW("Hardware backing HAL too slow, could only write %zu of %zu frames",
                frames_written, frames);
    }

    /* Always consume all bytes */
    return bytes;
}

static int out_get_render_position(const struct audio_stream_out *stream, uint32_t *dsp_frames) {
    *dsp_frames = 0;
    return -EINVAL;
}

static int out_standby(struct audio_stream *stream) {
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    pthread_mutex_lock(&out->lock);
    ALOGD("%s: out_standby: %s", __func__, out->bus_address);
    if (!out->standby) out->standby = true;
    pthread_mutex_unlock(&out->lock);
    return 0;
}

static int out_add_audio_effect(const struct audio_stream *stream, effect_handle_t effect) {
    // out_add_audio_effect is a no op
    return 0;
}

static int out_remove_audio_effect(const struct audio_stream *stream, effect_handle_t effect) {
    // out_remove_audio_effect is a no op
    return 0;
}

static int out_get_next_write_timestamp(const struct audio_stream_out *stream,
        int64_t *timestamp) {
    return -ENOSYS;
}

static uint32_t in_get_sample_rate(const struct audio_stream *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    return in->req_config.sample_rate;
}

static int in_set_sample_rate(struct audio_stream *stream, uint32_t rate) {
    return -ENOSYS;
}

static int refine_output_parameters(uint32_t *sample_rate, audio_format_t *format,
        audio_channel_mask_t *channel_mask) {
    static const uint32_t sample_rates [] = {
        8000, 16000, 48000
    };
    static const int sample_rates_count = sizeof(sample_rates)/sizeof(uint32_t);
    bool inval = false;
    if (*format != AUDIO_FORMAT_PCM_16_BIT) {
        *format = AUDIO_FORMAT_PCM_16_BIT;
        inval = true;
    }

    int channel_count = popcount(*channel_mask);
    if (channel_count != 1 && channel_count != 2) {
        *channel_mask = AUDIO_CHANNEL_IN_STEREO;
        inval = true;
    }

    int i;
    for (i = 0; i < sample_rates_count; i++) {
        if (*sample_rate < sample_rates[i]) {
            *sample_rate = sample_rates[i];
            inval=true;
            break;
        }
        else if (*sample_rate == sample_rates[i]) {
            break;
        }
        else if (i == sample_rates_count-1) {
            // Cap it to the highest rate we support
            *sample_rate = sample_rates[i];
            inval=true;
        }
    }

    if (inval) {
        return -EINVAL;
    }
    return 0;
}

static int refine_input_parameters(uint32_t *sample_rate, audio_format_t *format,
        audio_channel_mask_t *channel_mask) {
    static const uint32_t sample_rates [] = {8000, 16000, 48000};
    static const int sample_rates_count = sizeof(sample_rates)/sizeof(uint32_t);
    bool inval = false;
    // Only PCM_16_bit is supported. If this is changed, stereo to mono drop
    // must be fixed in in_read
    if (*format != AUDIO_FORMAT_PCM_16_BIT) {
        *format = AUDIO_FORMAT_PCM_16_BIT;
        inval = true;
    }

    int channel_count = popcount(*channel_mask);
    if (channel_count != 1 && channel_count != 2) {
        *channel_mask = AUDIO_CHANNEL_IN_STEREO;
        inval = true;
    }

    int i;
    for (i = 0; i < sample_rates_count; i++) {
        if (*sample_rate < sample_rates[i]) {
            *sample_rate = sample_rates[i];
            inval=true;
            break;
        }
        else if (*sample_rate == sample_rates[i]) {
            break;
        }
        else if (i == sample_rates_count-1) {
            // Cap it to the highest rate we support
            *sample_rate = sample_rates[i];
            inval=true;
        }
    }

    if (inval) {
        return -EINVAL;
    }
    return 0;
}

static size_t get_input_buffer_size(uint32_t sample_rate, audio_format_t format,
        audio_channel_mask_t channel_mask) {
    size_t size;
    int channel_count = popcount(channel_mask);
    if (refine_input_parameters(&sample_rate, &format, &channel_mask) != 0)
        return 0;

    size = sample_rate*IN_PERIOD_MS/1000;
    // Audioflinger expects audio buffers to be multiple of 16 frames
    size = ((size + 15) / 16) * 16;
    size *= sizeof(short) * channel_count;

    return size;
}

static size_t in_get_buffer_size(const struct audio_stream *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    int size = get_input_buffer_size(in->req_config.sample_rate,
                                 in->req_config.format,
                                 in->req_config.channel_mask);

    return size;
}

static audio_channel_mask_t in_get_channels(const struct audio_stream *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    return in->req_config.channel_mask;
}

static audio_format_t in_get_format(const struct audio_stream *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    return in->req_config.format;
}

static int in_set_format(struct audio_stream *stream, audio_format_t format) {
    return -ENOSYS;
}

static int in_dump(const struct audio_stream *stream, int fd) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;

    pthread_mutex_lock(&in->lock);
    dprintf(fd, "\tin_dump:\n"
                "\t\tsample rate: %u\n"
                "\t\tbuffer size: %zu\n"
                "\t\tchannel mask: %08x\n"
                "\t\tformat: %d\n"
                "\t\tdevice: %08x\n"
                "\t\taudio dev: %p\n\n",
                in_get_sample_rate(stream),
                in_get_buffer_size(stream),
                in_get_channels(stream),
                in_get_format(stream),
                in->device,
                in->dev);
    pthread_mutex_unlock(&in->lock);
    return 0;
}

static int in_set_parameters(struct audio_stream *stream, const char *kvpairs) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    struct str_parms *parms;
    char value[32];
    int ret;
    long val;
    char *end;

    pthread_mutex_lock(&in->lock);
    if (!in->standby) {
        ret = -ENOSYS;
    } else {
        parms = str_parms_create_str(kvpairs);

        ret = str_parms_get_str(parms, AUDIO_PARAMETER_STREAM_ROUTING,
                                value, sizeof(value));
        if (ret >= 0) {
            errno = 0;
            val = strtol(value, &end, 10);
            if ((errno == 0) && (end != NULL) && (*end == '\0') && ((int)val == val)) {
                in->device = (int)val;
                ret = 0;
            } else {
                ret = -EINVAL;
            }
        }

        str_parms_destroy(parms);
    }
    pthread_mutex_unlock(&in->lock);
    return ret;
}

static char *in_get_parameters(const struct audio_stream *stream, const char *keys) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    struct str_parms *query = str_parms_create_str(keys);
    char *str;
    char value[256];
    struct str_parms *reply = str_parms_create();
    int ret;

    ret = str_parms_get_str(query, AUDIO_PARAMETER_STREAM_ROUTING, value, sizeof(value));
    if (ret >= 0) {
        str_parms_add_int(reply, AUDIO_PARAMETER_STREAM_ROUTING, in->device);
        str = strdup(str_parms_to_str(reply));
    } else {
        str = strdup(keys);
    }

    str_parms_destroy(query);
    str_parms_destroy(reply);
    return str;
}

bool try_mixer(struct generic_audio_device *adev){
    if (!adev->mixer) adev->mixer = mixer_open(CARD);
    if (!adev->mixer) return false;
    return true;
}

int dev_set_mic_gain(struct generic_audio_device *adev, float gain){
    if (!try_mixer(adev)) return -1;

    pthread_mutex_lock(&adev->lock);

    struct mixer_ctl *vol_ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MIC_GAIN);
    if (vol_ctl != NULL){
    	int max = mixer_ctl_get_range_max(vol_ctl);
        mixer_ctl_set_value(vol_ctl, 0, max * gain);
    }

    pthread_mutex_unlock(&adev->lock);

    return 0;
}

static int in_set_gain(struct audio_stream_in *stream, float gain)
{
    return dev_set_mic_gain(((struct generic_stream_in *)stream)->dev, gain);
}

// Call with in->lock held
static void get_current_input_position(struct generic_stream_in *in,
        int64_t * position, struct timespec * timestamp) {
    struct timespec t = { .tv_sec = 0, .tv_nsec = 0 };
    clock_gettime(CLOCK_MONOTONIC, &t);
    const int64_t now_us = (t.tv_sec * 1000000000LL + t.tv_nsec) / 1000;
    if (timestamp) {
        *timestamp = t;
    }
    int64_t position_since_standby;
    if (in->standby) {
        position_since_standby = 0;
    } else {
        const int64_t first_us = (in->standby_exit_time.tv_sec * 1000000000LL +
                                  in->standby_exit_time.tv_nsec) / 1000;
        position_since_standby = (now_us - first_us) *
                in_get_sample_rate(&in->stream.common) /
                1000000;
        if (position_since_standby < 0) {
            position_since_standby = 0;
        }
    }
    *position = in->standby_position + position_since_standby;
}

// Must be called with in->lock held
static void do_in_standby(struct generic_stream_in *in) {
    if (in->standby) {
        return;
    }
    get_current_input_position(in, &in->standby_position, NULL);
    in->standby = true;
}

static int in_standby(struct audio_stream *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    pthread_mutex_lock(&in->lock);
    do_in_standby(in);
    pthread_mutex_unlock(&in->lock);
    return 0;
}

static ssize_t in_read(struct audio_stream_in *stream, void *buffer, size_t bytes) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    struct generic_audio_device *adev = in->dev;
    const size_t frames =  bytes / audio_stream_in_frame_size(stream);
    bool mic_mute = false;
    size_t read_bytes = 0;

    bool hfp_active = false;
    pthread_mutex_lock(&hfp_lock);
    if (hfp_thread != 0) hfp_active = true;
    pthread_mutex_unlock(&hfp_lock);
    if (hfp_active) return 0;

    adev_get_mic_mute(&adev->device, &mic_mute);
    pthread_mutex_lock(&in->lock);

    int64_t current_position;
    struct timespec current_time;

    get_current_input_position(in, &current_position, &current_time);
    if (in->standby) {
        in->standby = false;
        in->standby_exit_time = current_time;
        in->standby_frames_read = 0;
    }

    const int64_t frames_available =
        current_position - in->standby_position - in->standby_frames_read;
    assert(frames_available >= 0);

    const size_t frames_wait =
        ((uint64_t)frames_available > frames) ? 0 : frames - frames_available;

    int64_t sleep_time_us  = frames_wait * 1000000LL / in_get_sample_rate(&stream->common);

    pthread_mutex_unlock(&in->lock);

    if (sleep_time_us > 0) {
        usleep(sleep_time_us);
    }

    pthread_mutex_lock(&in->lock);
    int read_frames = 0;
    if (in->standby) {
        ALOGW("Input put to sleep while read in progress");
        goto exit;
    }
    in->standby_frames_read += frames;

    if (popcount(in->req_config.channel_mask) == 1 &&
        in->pcm_config.channels == 2) {
        // Need to resample to mono
        if (in->stereo_to_mono_buf_size < bytes*2) {
            in->stereo_to_mono_buf = realloc(in->stereo_to_mono_buf, bytes*2);
            if (!in->stereo_to_mono_buf) {
                ALOGE("Failed to allocate stereo_to_mono_buff");
                goto exit;
            }
        }

        read_frames = 0; //TODO audio_vbuffer_read(&in->buffer, in->stereo_to_mono_buf, frames);

        // Currently only pcm 16 is supported.
        uint16_t *src = (uint16_t *)in->stereo_to_mono_buf;
        uint16_t *dst = (uint16_t *)buffer;
        size_t i;
        // Resample stereo 16 to mono 16 by dropping one channel.
        // The stereo stream is interleaved L-R-L-R
        for (i = 0; i < frames; i++) {
            *dst = *src;
            src += 2;
            dst += 1;
        }
    } else
        read_frames = 0; // TODO audio_vbuffer_read(&in->buffer, buffer, frames);

exit:
    read_bytes = read_frames*audio_stream_in_frame_size(stream);

    if (mic_mute) {
        read_bytes = 0;
    }

    if (read_bytes < bytes) {
        memset (&((uint8_t *)buffer)[read_bytes], 0, bytes-read_bytes);
    }

    pthread_mutex_unlock(&in->lock);

    return bytes;
}

static uint32_t in_get_input_frames_lost(struct audio_stream_in *stream) {
    return 0;
}

static int in_get_capture_position(const struct audio_stream_in *stream,
        int64_t *frames, int64_t *time) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    pthread_mutex_lock(&in->lock);
    struct timespec current_time;
    get_current_input_position(in, frames, &current_time);
    *time = (current_time.tv_sec * 1000000000LL + current_time.tv_nsec);
    pthread_mutex_unlock(&in->lock);
    return 0;
}

static int in_add_audio_effect(const struct audio_stream *stream, effect_handle_t effect) {
    // in_add_audio_effect is a no op
    return 0;
}

static int in_remove_audio_effect(const struct audio_stream *stream, effect_handle_t effect) {
    // in_add_audio_effect is a no op
    return 0;
}

static int adev_open_output_stream(struct audio_hw_device *dev,
        audio_io_handle_t handle, audio_devices_t devices, audio_output_flags_t flags,
        struct audio_config *config, struct audio_stream_out **stream_out, const char *address) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    struct generic_stream_out *out;
    int ret = 0;

    if (refine_output_parameters(&config->sample_rate, &config->format, &config->channel_mask)) {
        ALOGE("Error opening output stream format %d, channel_mask %04x, sample_rate %u",
              config->format, config->channel_mask, config->sample_rate);
        ret = -EINVAL;
        goto error;
    }

    out = (struct generic_stream_out *)calloc(1, sizeof(struct generic_stream_out));

    if (!out)
        return -ENOMEM;

    out->stream.common.get_sample_rate = out_get_sample_rate;
    out->stream.common.set_sample_rate = out_set_sample_rate;
    out->stream.common.get_buffer_size = out_get_buffer_size;
    out->stream.common.get_channels = out_get_channels;
    out->stream.common.get_format = out_get_format;
    out->stream.common.set_format = out_set_format;
    out->stream.common.standby = out_standby;
    out->stream.common.dump = out_dump;
    out->stream.common.set_parameters = out_set_parameters;
    out->stream.common.get_parameters = out_get_parameters;
    out->stream.common.add_audio_effect = out_add_audio_effect;
    out->stream.common.remove_audio_effect = out_remove_audio_effect;
    out->stream.get_latency = out_get_latency;
    out->stream.set_volume = out_set_volume;
    out->stream.write = out_write;
    out->stream.get_render_position = out_get_render_position;
    out->stream.get_next_write_timestamp = out_get_next_write_timestamp;

    pthread_mutex_init(&out->lock, (const pthread_mutexattr_t *) NULL);
    out->dev = adev;
    out->device = devices;
    memcpy(&out->req_config, config, sizeof(struct audio_config));
    memcpy(&out->pcm_config, &pcm_config_out, sizeof(struct pcm_config));
    out->pcm_config.rate = config->sample_rate;
    //out->pcm_config.period_size = out->pcm_config.rate*OUT_PERIOD_MS/1000;

    out->standby = true;

    if (address) {
        out->bus_address = calloc(strlen(address) + 1, sizeof(char));
        strncpy((char *)out->bus_address, address, strlen(address));
        hashmapPut(adev->out_bus_stream_map, (void *)out->bus_address, out);
        /* TODO: read struct audio_gain from audio_policy_configuration */
        out->gain_stage = (struct audio_gain) {
            .min_value = -10350,
            .max_value = 2400,
            .step_value = 50,
        };
        out->amplitude_ratio = 1.0;
    }
    *stream_out = &out->stream;
    ALOGD("%s bus:%s", __func__, out->bus_address);

error:
    return ret;
}

static void adev_close_output_stream(struct audio_hw_device *dev,
        struct audio_stream_out *stream) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    struct generic_stream_out *out = (struct generic_stream_out *)stream;
    ALOGD("%s bus:%s", __func__, out->bus_address);
    pthread_mutex_lock(&out->lock);
    if (!out->standby) out->standby = true;

    if (out->bus_address) {
        hashmapRemove(adev->out_bus_stream_map, (void *)out->bus_address);
        free((void *)out->bus_address);
    }
    free(stream);
}

static int adev_set_master_mute(struct audio_hw_device *dev, bool muted) {
    ALOGD("%s: %s", __func__, _bool_str(muted));

    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    if (!try_mixer(adev)) return -1;

    pthread_mutex_lock(&adev->lock);

    struct mixer_ctl *ctl;
    adev->master_mute = muted;

    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_FRONT_UNMUTE);
    if (ctl != NULL){
        mixer_ctl_set_value(ctl, 0, !muted);
        mixer_ctl_set_value(ctl, 1, !muted);
    }

    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_REAR_UNMUTE);
    if (ctl != NULL){
        if (hfp_thread != 0){
            // Rear speakers muted when HFP call is active.
            mixer_ctl_set_value(ctl, 0, 0);
            mixer_ctl_set_value(ctl, 1, 0);
        } else {
            mixer_ctl_set_value(ctl, 0, !muted);
            mixer_ctl_set_value(ctl, 1, !muted);
        }
    }

    pthread_mutex_unlock(&adev->lock);
    return 0;
}

static int adev_get_master_mute(struct audio_hw_device *dev, bool *muted) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    pthread_mutex_lock(&adev->lock);
    *muted = adev->master_mute;
    pthread_mutex_unlock(&adev->lock);
    ALOGD("%s: %s", __func__, _bool_str(*muted));
    return 0;
}

void set_volume(struct audio_hw_device *dev)
{
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    if (!try_mixer(adev)) return;
    pthread_mutex_lock(&adev->lock);

    struct mixer_ctl *vol_ctl_front = mixer_get_ctl_by_name(adev->mixer, MIXER_FRONT_GAIN);
    struct mixer_ctl *vol_ctl_rear = mixer_get_ctl_by_name(adev->mixer, MIXER_REAR_GAIN);

    int gain = media_gain;
    if (hfp_thread != 0) gain = hfp_gain;

    if (vol_ctl_front != NULL){
        mixer_ctl_set_value(vol_ctl_front, 0, gain);
        mixer_ctl_set_value(vol_ctl_front, 1, gain);
    }
    if (vol_ctl_rear != NULL){
        gain -= 5;
        mixer_ctl_set_value(vol_ctl_rear, 0, gain);
        mixer_ctl_set_value(vol_ctl_rear, 1, gain);
    }

    pthread_mutex_unlock(&adev->lock);
}

bool set_radio_patch(struct audio_hw_device *dev, bool enable){
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    if (!try_mixer(adev)) return false;

    bool radio_on = false;

    pthread_mutex_lock(&adev->lock);

    struct mixer_ctl *line_in_ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_RADIO_SOURCE);
    if (line_in_ctl != NULL){
        radio_on = (mixer_ctl_get_value(line_in_ctl, 0) == 2);
        mixer_ctl_set_value(line_in_ctl, 0, enable ? MIXER_RADIO_EN : MIXER_RADIO_DIS);
    }

    pthread_mutex_unlock(&adev->lock);
    return radio_on;
}

/////////////////////////////////////////////////////////////////////
// HFP related stuff starts HERE
/////////////////////////////////////////////////////////////////////

size_t min(size_t x, size_t y){ 
    return y ^ ((x ^ y) & -(x < y)); 
}

/*size_t max(size_t x, size_t y) { 
    return x ^ ((x ^ y) & -(x < y));  
}*/

void* hfp_run(void * args) {

    int sleeper, webrtc_debug, loopcounter = 0;
    bool unmuted = false, radio_on;

    static struct pcm *hfp_pcm_in = NULL;
    static struct pcm *hfp_pcm_out = NULL;
    static struct pcm *sc_pcm_in = NULL;
    static struct pcm *sc_pcm_out = NULL;

    uint8_t delay_cnt = 0;
    int median, std;

    struct generic_audio_device * adev = (struct generic_audio_device *)args;

    radio_on = set_radio_patch((struct audio_hw_device *)adev, false);

    struct timespec hwtime;
    unsigned int f_read_avail;
    unsigned int f_write_avail;

    // A block is 10 ms long, and 2 bytes per sample.
    size_t block_len = 2 * HFP_LOCAL_RATE / 100;
    int16_t* framebuf = (int16_t *)malloc(block_len);

    int f_ms = HFP_LOCAL_RATE / 1000;
    size_t buff_s = 1024 * 16;

    struct pcm_config mconfig = {
        .channels = 1,
        .rate = HFP_LOCAL_RATE,
        .format = PCM_FORMAT_S16_LE,
        .period_size = 1024,
        .period_count = 4,
        .start_threshold = 0, // 0's mean default
        .silence_threshold = 0,
        .stop_threshold = 0,
    };

    pthread_mutex_lock(&pcm_lock);
    if (pcm) pcm_close(pcm);
    pthread_mutex_unlock(&pcm_lock);

    sleeper = 0;
    while ((sc_pcm_in = pcm_open(CARD, RECORD_DEVICE, PCM_IN, &mconfig)) == 0 && sleeper < 10) usleep(50);
    if (sc_pcm_in == 0) {
        ALOGD("%s: failed to allocate memory for SC PCM in", __func__);
        goto error;
    } else if (!pcm_is_ready(sc_pcm_in)){
        ALOGD("%s: failed to open SC PCM in (c/d: %d/%d): %s", __func__, CARD, RECORD_DEVICE, pcm_get_error(sc_pcm_in));
        goto error;
    }

    sleeper = 0;
    while ((sc_pcm_out = pcm_open(CARD, PLAYBACK_DEVICE, PCM_OUT, &mconfig)) == 0 && sleeper < 10) usleep(50);
    if (sc_pcm_out == 0) {
        ALOGD("%s: failed to allocate memory for SC PCM in", __func__);
        goto error;
    } else if (!pcm_is_ready(sc_pcm_out)){
        ALOGD("%s: failed to open SC PCM out (c/d: %d/%d): %s", __func__, CARD, PLAYBACK_DEVICE, pcm_get_error(sc_pcm_out));
        goto error;
    }

    sleeper = 0;
    while ((hfp_pcm_in = pcm_open(CARD, SCO_RECORD_DEVICE, PCM_IN, &mconfig)) == 0 && sleeper < 10) usleep(50);
    if (hfp_pcm_in == 0) {
        ALOGD("%s: failed to allocate memory for HFP PCM in", __func__);
        goto error;
    } else if (!pcm_is_ready(hfp_pcm_in)){
        ALOGD("%s: failed to open HFP PCM in (c/d: %d/%d): %s", __func__, CARD, SCO_RECORD_DEVICE, pcm_get_error(hfp_pcm_in));
        goto error;
    }

    sleeper = 0;
    while ((hfp_pcm_out = pcm_open(CARD, SCO_PLAYBACK_DEVICE, PCM_OUT, &mconfig)) == 0 && sleeper < 10) usleep(50);
    if (hfp_pcm_out == 0) {
        ALOGD("%s: failed to allocate memory for HFP PCM out", __func__);
        goto error;
    } else if (!pcm_is_ready(hfp_pcm_out)){
        ALOGD("%s: failed to open HFP PCM out (c/d: %d/%d): %s", __func__, CARD, SCO_PLAYBACK_DEVICE, pcm_get_error(hfp_pcm_out));
        goto error;
    }

    if (framebuf == NULL) {
        ALOGD("%s: failed to allocate frame buffers", __func__);
        goto error;
    }

    // AudioProcessing: Initialize
    struct audioproc *apm = audioproc_create();
    struct audioframe *frame = audioframe_create(1, HFP_LOCAL_RATE, HFP_LOCAL_RATE / 100);

    // AudioProcessing: Setup
    audioproc_hpf_en(apm, 1);
    audioproc_aec_drift_comp_en(apm, 0);
    audioproc_aec_en(apm, 1);
    audioproc_aec_delay_logging_en(apm, 1);
    audioproc_aec_delayag_en(apm);
    audioproc_ns_set_level(apm, 2); // 0 = low, 1 = moderate, 2 = high, 3 = veryhigh
    audioproc_ns_en(apm, 1);
    audioproc_agc_set_level_limits(apm, 0, 255);
    audioproc_agc_set_mode(apm, 1); // 0 = Adaptive Analog, 1 = Adaptive Digital, 2 = Fixed Digital
    audioproc_agc_en(apm, 1);

    ALOGD("%s: PCM loop starting", __func__);

    /*  The timing of the entire system is controlled by the bluetooth pcm.
     *  We perform blocking reads of 10 ms, which is the block length demanded
     *  by webrtc, do our local processing, then write precisely 10 ms back out
     *  to the bluetooth pcm. The only possible way that this could allow
     *  buffering problems is if the processing time for a 10 ms block exceeded
     *  10 ms.
     *
     *  We know that the output will always have capacity since it has the same
     *  clock as the input.
     */
    while (!term_hfp && pcm_read(hfp_pcm_in, framebuf, block_len) == 0){

        // 500 ms delayed unmute to skip the initial pop.
        if (!unmuted && loopcounter > 50){
            set_volume((struct audio_hw_device *)adev);
            adev_set_master_mute((struct audio_hw_device *)adev, false);
            unmuted = true;
        }

        // Record AEC metrics to log
        delay_cnt++;
        delay_cnt %= 100;
        if (delay_cnt == 0){
            audioproc_aec_get_delay_metrics(apm, &median, &std);
            ALOGD("%s: AEC DELAY METRICS. MEDIAN: %03d, DEVIATION: %03d", __func__, median, std);
        }

        // AudioProcessing: Analyze reverse stream
        audioframe_setdata(frame, framebuf, block_len);
        audioproc_aec_echo_ref(apm, frame);

        // Get current buffer stats
	pcm_get_htimestamp(sc_pcm_out, &f_write_avail, &hwtime);
        pcm_get_htimestamp(sc_pcm_in, &f_read_avail, &hwtime);

        // Adjust data from mono to stereo, write to sound card, read from sound card, adjust data to mono
        pcm_write(sc_pcm_out, framebuf, min(block_len, 2 * min(f_write_avail, f_read_avail)));
        memset(framebuf, 0, block_len);
        pcm_read(sc_pcm_in, framebuf, min(block_len, 2 * min(f_write_avail, f_read_avail)));

        // AudioProcessing: Process Audio
        audioframe_setdata(frame, framebuf, block_len);
        // delay is the total number of frames in the hardware buffers, divided by the number of frames per millisecond.
        audioproc_aec_set_delay(apm, (buff_s - f_write_avail + f_read_avail) / f_ms);
        webrtc_debug = audioproc_process(apm, frame);
        audioframe_getdata(frame, framebuf, block_len);

        if (webrtc_debug != 0) ALOGE("%s: Webrtc error code: %d", __func__, webrtc_debug);

        // Write debug data to log for first 10ms * 1000 = 10s.
        if (loopcounter < 1000){
            ALOGD("%s: near out avail: %04d, near in avail: %04d", __func__, f_write_avail, f_read_avail);
            pcm_get_htimestamp(hfp_pcm_out, &f_write_avail, &hwtime);
            pcm_get_htimestamp(hfp_pcm_in, &f_read_avail, &hwtime);
            ALOGD("%s: hfp  out avail: %04d, hfp  in avail: %04d", __func__, f_write_avail, f_read_avail);
            loopcounter++;
        }

        // convert to stereo and send to BT
        pcm_write(hfp_pcm_out, framebuf, block_len);
    }

    ALOGD("%s: PCM loop terminated", __func__);

    // AudioProcessing: Done
    audioproc_destroy(apm);

error:
    if (sc_pcm_in) pcm_close(sc_pcm_in);
    if (hfp_pcm_in) pcm_close(hfp_pcm_in);
    if (hfp_pcm_out) pcm_close(hfp_pcm_out);
    if (sc_pcm_out) pcm_close(sc_pcm_out);

    free(framebuf);

    pthread_mutex_lock(&hfp_lock);
    hfp_thread = 0;
    on_phone = false;
    pthread_mutex_unlock(&hfp_lock);

    set_volume((struct audio_hw_device *)adev);
    if (radio_on) set_radio_patch((struct audio_hw_device *)adev, true);

    return NULL;
}

static int adev_set_parameters(struct audio_hw_device *dev, const char *kvpairs) {
    ALOGD("%s: kvpairs: %s", __func__, kvpairs);

    struct audio_device * adev = (struct audio_device *)dev;
    char value[32];
    int ret, val = 0;
    struct str_parms *parms;

    parms = str_parms_create_str(kvpairs);

    pthread_mutex_lock(&hfp_lock);

    ret = str_parms_get_str(parms, AUDIO_PARAMETER_HFP_SET_SAMPLING_RATE, value, sizeof(value));
    if (ret >= 0) {
        val = atoi(value);

        struct mixer_ctl *ctl = mixer_get_ctl_by_name(((struct generic_audio_device *)adev)->mixer, "PRI_PCM SampleRate");
	if (ctl != NULL) mixer_ctl_set_enum_by_string(ctl, val == 16000 ? "16 kHz" : "8 kHz");

    }

    ret = str_parms_get_str(parms, AUDIO_PARAMETER_HFP_ENABLE, value, sizeof(value));
    if (ret >= 0) {
        if (strcmp(value, "true") == 0){
            if (hfp_thread == 0) {
                adev_set_master_mute(dev, true);
		sleep(1); // sleep to give the DAC time to mute.
                dev_set_mic_gain((struct generic_audio_device *)dev, MIXER_MIC_GAIN_DEFAULT);
                term_hfp = false;
		on_phone = true;
                pthread_create(&hfp_thread, NULL, &hfp_run, adev);
            }
        } else {
            if (hfp_thread != 0) {
                adev_set_master_mute(dev, true);
                sleep(2); // sleep to give the DAC time to mute
                term_hfp = true; // this will cause the thread to exit the main loop and terminate.
                set_volume(dev);
		sleep(1); // to give time for hfp thread to term so that back speakers are unmuted.
                adev_set_master_mute(dev, false);
            }
        }
    }

    pthread_mutex_unlock(&hfp_lock);

    return 0;
}

/////////////////////////////////////////////////////////////////////
// HFP related stuff ends HERE
/////////////////////////////////////////////////////////////////////

static char *adev_get_parameters(const struct audio_hw_device *dev, const char *keys) {
    return NULL;
}

static int adev_init_check(const struct audio_hw_device *dev) {
    return 0;
}

static int adev_set_voice_volume(struct audio_hw_device *dev, float volume) {
    // adev_set_voice_volume is a no op (simulates phones)
    return 0;
}

static int adev_set_master_volume(struct audio_hw_device *dev, float volume) {
    return -ENOSYS;
}

static int adev_get_master_volume(struct audio_hw_device *dev, float *volume) {
    return -ENOSYS;
}

static int adev_set_mode(struct audio_hw_device *dev, audio_mode_t mode) {
    // adev_set_mode is a no op (simulates phones)
    return 0;
}

static int adev_set_mic_mute(struct audio_hw_device *dev, bool state) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    if (!try_mixer(adev)) return -1;
    pthread_mutex_lock(&adev->lock);

    adev->mic_mute = state;

    struct mixer_ctl *mic_mute_ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MIC_MUTE);
    if (mic_mute_ctl != NULL){
        if (state){
            mixer_ctl_set_value(mic_mute_ctl, 0, 1);
        } else {
            mixer_ctl_set_value(mic_mute_ctl, 0, 0);
        }
    }

    pthread_mutex_unlock(&adev->lock);
    return 0;
}

static int adev_get_mic_mute(const struct audio_hw_device *dev, bool *state) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    pthread_mutex_lock(&adev->lock);
    *state = adev->mic_mute;
    pthread_mutex_unlock(&adev->lock);
    return 0;
}

static size_t adev_get_input_buffer_size(const struct audio_hw_device *dev,
        const struct audio_config *config) {
    return get_input_buffer_size(config->sample_rate, config->format, config->channel_mask);
}

static void adev_close_input_stream(struct audio_hw_device *dev,
        struct audio_stream_in *stream) {
    struct generic_stream_in *in = (struct generic_stream_in *)stream;
    pthread_mutex_lock(&in->lock);
    do_in_standby(in);

    pthread_mutex_unlock(&in->lock);

    if (in->stereo_to_mono_buf != NULL) {
        free(in->stereo_to_mono_buf);
        in->stereo_to_mono_buf_size = 0;
    }

    if (in->bus_address) {
        free((void *)in->bus_address);
    }

    pthread_mutex_destroy(&in->lock);
    free(stream);
}

static int adev_open_input_stream(struct audio_hw_device *dev,
        audio_io_handle_t handle, audio_devices_t devices, struct audio_config *config,
        struct audio_stream_in **stream_in, audio_input_flags_t flags __unused, const char *address,
        audio_source_t source __unused) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    struct generic_stream_in *in;
    int ret = 0;
    if (refine_input_parameters(&config->sample_rate, &config->format, &config->channel_mask)) {
        ALOGE("Error opening input stream format %d, channel_mask %04x, sample_rate %u",
              config->format, config->channel_mask, config->sample_rate);
        ret = -EINVAL;
        goto error;
    }

    in = (struct generic_stream_in *)calloc(1, sizeof(struct generic_stream_in));
    if (!in) {
        ret = -ENOMEM;
        goto error;
    }

    in->stream.common.get_sample_rate = in_get_sample_rate;
    in->stream.common.set_sample_rate = in_set_sample_rate;         // no op
    in->stream.common.get_buffer_size = in_get_buffer_size;
    in->stream.common.get_channels = in_get_channels;
    in->stream.common.get_format = in_get_format;
    in->stream.common.set_format = in_set_format;                   // no op
    in->stream.common.standby = in_standby;
    in->stream.common.dump = in_dump;
    in->stream.common.set_parameters = in_set_parameters;
    in->stream.common.get_parameters = in_get_parameters;
    in->stream.common.add_audio_effect = in_add_audio_effect;       // no op
    in->stream.common.remove_audio_effect = in_remove_audio_effect; // no op
    in->stream.set_gain = in_set_gain;
    in->stream.read = in_read;
    in->stream.get_input_frames_lost = in_get_input_frames_lost;    // no op
    in->stream.get_capture_position = in_get_capture_position;

    pthread_mutex_init(&in->lock, (const pthread_mutexattr_t *) NULL);
    in->dev = adev;
    in->device = devices;
    memcpy(&in->req_config, config, sizeof(struct audio_config));
    memcpy(&in->pcm_config, &pcm_config_in, sizeof(struct pcm_config));
    in->pcm_config.rate = config->sample_rate;
    in->pcm_config.period_size = 1024;//in->pcm_config.rate*IN_PERIOD_MS/1000;

    in->stereo_to_mono_buf = NULL;
    in->stereo_to_mono_buf_size = 0;

    in->standby = true;
    in->standby_position = 0;
    in->standby_exit_time.tv_sec = 0;
    in->standby_exit_time.tv_nsec = 0;
    in->standby_frames_read = 0;

    if (address) {
        in->bus_address = calloc(strlen(address) + 1, sizeof(char));
        strncpy((char *)in->bus_address, address, strlen(address));
    }

    *stream_in = &in->stream;

error:
    return ret;
}

static int adev_dump(const audio_hw_device_t *dev, int fd) {
    return 0;
}

static int adev_set_audio_port_config(struct audio_hw_device *dev,
        const struct audio_port_config *config) {
    int ret = 0;
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    const char *bus_address = config->ext.device.address;
    struct generic_stream_out *out = hashmapGet(adev->out_bus_stream_map, (void *)bus_address);
    if (out) {
        pthread_mutex_lock(&out->lock);
        int gainIndex = (config->gain.values[0] - out->gain_stage.min_value) /
            out->gain_stage.step_value;
        int totalSteps = (out->gain_stage.max_value - out->gain_stage.min_value) /
            out->gain_stage.step_value;
        int minDb = out->gain_stage.min_value / 100;
        int maxDb = out->gain_stage.max_value / 100;
        // curve: 10^((minDb + (maxDb - minDb) * gainIndex / totalSteps) / 20)
        out->amplitude_ratio = pow(10,
                (minDb + (maxDb - minDb) * (gainIndex / (float)totalSteps)) / 20);

        // The mixer has input from 0-255 inclusive.
        // 0 is mute, 1 is -103 dB,
        // steps of 1 correspond to 0.5 dB
        // such that 207 = 0 dB, 255 = +24.0 dB.
        // We have input values into the audio policy in mB as;
        // min: -8400 -- note our real min is -10350
        // max: 2400
        // default: -1800
        // step: 50
        // Note: each 6 dB corresponds to ~50% "sound pressure". -1800 mB =
        // -18 dB = -6x3 dB = ~0.5^3 or about 12.5% of the sound pressure
        // compared to 0 dB.
        if (strcmp(bus_address, "bus0_media_out") == 0) media_gain = gainIndex;
        if (strcmp(bus_address, "bus4_call_out") == 0) hfp_gain = gainIndex;

        pthread_mutex_unlock(&out->lock);
        ALOGD("%s: set audio gain: %f %% / %d dB on %s",
                __func__, out->amplitude_ratio, gainIndex, bus_address);
        set_volume(dev);
    } else {
        ALOGE("%s: can not find output stream by bus_address:%s", __func__, bus_address);
        ret = -EINVAL;
    }
    return ret;
}

static int adev_create_audio_patch(struct audio_hw_device *dev,
        unsigned int num_sources,
        const struct audio_port_config *sources,
        unsigned int num_sinks,
        const struct audio_port_config *sinks,
        audio_patch_handle_t *handle) {
    struct generic_audio_device *audio_dev = (struct generic_audio_device *)dev;
    bool isTuner = false;
    bool isBus0 = false;
    for (int i = 0; i < num_sources; i++) {
        ALOGD("%s: source[%d] type=%d address=%s", __func__, i, sources[i].type,
                sources[i].type == AUDIO_PORT_TYPE_DEVICE
                ? sources[i].ext.device.address
                : "");
        if (sources[i].type == AUDIO_PORT_TYPE_DEVICE
                && strcmp(sources[i].ext.device.address, "tuner0") == 0)
            isTuner = true;
    }
    for (int i = 0; i < num_sinks; i++) {
        ALOGD("%s: sink[%d] type=%d address=%s", __func__, i, sinks[i].type,
                sinks[i].type == AUDIO_PORT_TYPE_DEVICE ? sinks[i].ext.device.address
                : "N/A");
//        if (sinks[i].type == AUDIO_PORT_TYPE_DEVICE
//                && strcmp(sinks[i].ext.device.address, "bus0_media_out") == 0)
            isBus0 = true;
    }
    if (num_sources == 1 && num_sinks == 1 &&
            sources[0].type == AUDIO_PORT_TYPE_DEVICE &&
            sinks[0].type == AUDIO_PORT_TYPE_DEVICE) {
        pthread_mutex_lock(&audio_dev->lock);
        audio_dev->last_patch_id += 1;
        pthread_mutex_unlock(&audio_dev->lock);
        *handle = audio_dev->last_patch_id;
        ALOGD("%s: handle: %d", __func__, *handle);
    }

    if (isTuner && isBus0) set_radio_patch(dev, true);

    return 0;
}

static int adev_release_audio_patch(struct audio_hw_device *dev,
        audio_patch_handle_t handle) {
    ALOGD("%s: handle: %d", __func__, handle);
    set_radio_patch(dev, false);
    return 0;
}

static int adev_close(hw_device_t *dev) {
    struct generic_audio_device *adev = (struct generic_audio_device *)dev;
    int ret = 0;
    if (!adev)
        return 0;

    pthread_mutex_lock(&adev_init_lock);

    if (audio_device_ref_count == 0) {
        ALOGE("adev_close called when ref_count 0");
        ret = -EINVAL;
        goto error;
    }

    if ((--audio_device_ref_count) == 0) {
        if (adev->mixer) {
            mixer_close(adev->mixer);
        }
        if (adev->out_bus_stream_map) {
            hashmapFree(adev->out_bus_stream_map);
        }
        free(adev);
    }

error:
    pthread_mutex_unlock(&adev_init_lock);
    return ret;
}

/* copied from libcutils/str_parms.c */
static bool str_eq(void *key_a, void *key_b) {
    return !strcmp((const char *)key_a, (const char *)key_b);
}

/**
 * use djb hash unless we find it inadequate.
 * copied from libcutils/str_parms.c
 */
#ifdef __clang__
__attribute__((no_sanitize("integer")))
#endif
static int str_hash_fn(void *str) {
    uint32_t hash = 5381;
    char *p;
    for (p = str; p && *p; p++) {
        hash = ((hash << 5) + hash) + *p;
    }
    return (int)hash;
}

int carddev(const struct dirent *d){
    if (strstr(d->d_name, "card") != NULL) return 1;
    return 0;
}

static int adev_open(const hw_module_t *module,
        const char *name, hw_device_t **device) {
    static struct generic_audio_device *adev;
    int n;
    struct dirent **filelist;

    if (strcmp(name, AUDIO_HARDWARE_INTERFACE) != 0)
        return -EINVAL;

    pthread_mutex_lock(&adev_init_lock);
    if (audio_device_ref_count != 0) {
        *device = &adev->device.common;
        audio_device_ref_count++;
        ALOGV("%s: returning existing instance of adev", __func__);
        ALOGV("%s: exit", __func__);
        goto unlock;
    }
    adev = calloc(1, sizeof(struct generic_audio_device));

    pthread_mutex_init(&adev->lock, (const pthread_mutexattr_t *) NULL);

    adev->device.common.tag = HARDWARE_DEVICE_TAG;
    adev->device.common.version = AUDIO_DEVICE_API_VERSION_3_0;
    adev->device.common.module = (struct hw_module_t *) module;
    adev->device.common.close = adev_close;

    adev->device.init_check = adev_init_check;               // no op
    adev->device.set_voice_volume = adev_set_voice_volume;   // no op
    adev->device.set_master_volume = adev_set_master_volume; // no op
    adev->device.get_master_volume = adev_get_master_volume; // no op
    adev->device.set_master_mute = adev_set_master_mute;
    adev->device.get_master_mute = adev_get_master_mute;
    adev->device.set_mode = adev_set_mode;                   // no op
    adev->device.set_mic_mute = adev_set_mic_mute;
    adev->device.get_mic_mute = adev_get_mic_mute;
    adev->device.set_parameters = adev_set_parameters;       // no op
    adev->device.get_parameters = adev_get_parameters;       // no op
    adev->device.get_input_buffer_size = adev_get_input_buffer_size;
    adev->device.open_output_stream = adev_open_output_stream;
    adev->device.close_output_stream = adev_close_output_stream;
    adev->device.open_input_stream = adev_open_input_stream;
    adev->device.close_input_stream = adev_close_input_stream;
    adev->device.dump = adev_dump;

    // New in AUDIO_DEVICE_API_VERSION_3_0
    adev->device.set_audio_port_config = adev_set_audio_port_config;
    adev->device.create_audio_patch = adev_create_audio_patch;
    adev->device.release_audio_patch = adev_release_audio_patch;

    *device = &adev->device.common;

    // Find the card number
    // /sys/devices/platform/sound/sound/cardX
    n = scandir("/sys/devices/platform/sound/sound", &filelist, *carddev, NULL);
    ALOGD("%s: found %d platform sound cards.", __func__, n);
    if (n <= 0){
	    free(filelist);
	    ALOGE("%s: No cards, exiting with -ENODEV.", __func__);
	    return -ENODEV;
    }
    while (n--){
      ALOGD("%s: found card: %s.", __func__, filelist[n]->d_name);
      CARD = atoi(&filelist[n]->d_name[4]);
      free(filelist[n]);
    }
    free(filelist);
    ALOGD("%s: using CARD: %d.", __func__, CARD);

    // Initialize the sound card to get the clock running.
    struct pcm *pcm_init = NULL;
    pcm_init = pcm_open(CARD, PLAYBACK_DEVICE, PCM_OUT, &pcm_config_out);
    sleep(1);
    if (pcm_init) pcm_close(pcm_init);
    pcm_init = NULL;

    adev->mixer = mixer_open(CARD);

    // Set default mixer ctls
    // Enable channels and set volume
    struct mixer_ctl *ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_RADIO_GAIN);
    if (ctl != NULL){
        for (int z = 0; z < (int)mixer_ctl_get_num_values(ctl); z++) {
            ALOGD("set ctl %d to %d", z, MIXER_RADIO_GAIN_DEFAULT);
            mixer_ctl_set_value(ctl, z, MIXER_RADIO_GAIN_DEFAULT);
        }
    }

    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_FRONT_UNMUTE);
    if (ctl != NULL){
        mixer_ctl_set_value(ctl, 0, true);
        mixer_ctl_set_value(ctl, 1, true);
    }
    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_REAR_UNMUTE);
    if (ctl != NULL){
        mixer_ctl_set_value(ctl, 0, true);
        mixer_ctl_set_value(ctl, 1, true);
    }

    // Set up routing mixers
    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MM1);
    if (ctl != NULL) mixer_ctl_set_value(ctl, 0, true);
    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MM2);
    if (ctl != NULL) mixer_ctl_set_value(ctl, 0, true);

    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MM3);
    if (ctl != NULL) mixer_ctl_set_value(ctl, 0, true);
    ctl = mixer_get_ctl_by_name(adev->mixer, MIXER_MM4);
    if (ctl != NULL) mixer_ctl_set_value(ctl, 0, true);

    ctl = mixer_get_ctl_by_name(adev->mixer, "Setup DSP");
    if (ctl != NULL) mixer_ctl_set_value(ctl, 0, true);

    // Initialize the bus address to output stream map
    adev->out_bus_stream_map = hashmapCreate(5, str_hash_fn, str_eq);

    audio_device_ref_count++;

unlock:
    pthread_mutex_unlock(&adev_init_lock);
    return 0;
}

static struct hw_module_methods_t hal_module_methods = {
    .open = adev_open,
};

struct audio_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .module_api_version = AUDIO_MODULE_API_VERSION_0_1,
        .hal_api_version = HARDWARE_HAL_API_VERSION,
        .id = AUDIO_HARDWARE_MODULE_ID,
        .name = "Car audio HW HAL",
        .author = "The Android Open Source Project",
        .methods = &hal_module_methods,
    },
};
