/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "BroadcastRadioDefault.tuner"
#define LOG_NDEBUG 0

#include "BroadcastRadio.h"
#include "Tuner.h"

#include <broadcastradio-utils-1x/Utils.h>
#include <cutils/properties.h>
#include <log/log.h>

#include "TEF6686/lib/tef6686.h"

namespace android {
namespace hardware {
namespace broadcastradio {
namespace V1_1 {
namespace implementation {

using namespace std::chrono_literals;

using V1_0::Band;
using V1_0::BandConfig;
using V1_0::Class;
using V1_0::Direction;
using utils::HalRevision;

using std::chrono::milliseconds;
using std::lock_guard;
using std::move;
using std::mutex;
using std::sort;
using std::vector;

const struct {
    milliseconds config = 50ms;
    milliseconds scan = 200ms;
    milliseconds step = 100ms;
    milliseconds tune = 150ms;
    milliseconds rds = 100ms;
} gDefaultDelay;

Tuner::Tuner(V1_0::Class classId, const sp<V1_0::ITunerCallback>& callback)
    : mClassId(classId),
      mCallback(callback),
      mCallback1_1(ITunerCallback::castFrom(callback).withDefault(nullptr)),
      mIsAnalogForced(false) {

	int r = 0;
	char i2c_p[PROP_VALUE_MAX];

        ALOGD("Tuner::Tuner");

	property_get("persist.service.broadcastradio.i2c", i2c_p, "/dev/i2c-1");
	setup_i2c(i2c_p);

	while (r == 0){
		r = Tuner_Power_on(); // Check tuner status.
		// r ==  0: busy
		// r ==  1: idle or boot state, awaiting initialization
		// r ==  2: active, do not reinitialize
		// r == -1: error
		ALOGD("%s Radio_Power_on(): %d", __func__, r);
		if (r == -1) {
			ALOGE("%s Radio initialization error", __func__);
			return;
		}
	}

	if (r != 2){
		if (!Tuner_Init()){ // load tuner initialization.
			ALOGD("%s Radio bailing out", __func__);
			return;
		}
	}

	Radio_SetFreq(Radio_PRESETMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq()); // Tune to default channel.

	pthread_mutex_init(&radio_lock, (const pthread_mutexattr_t *) NULL);

	mIsClosed = false;
}

void Tuner::forceClose() {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    //TODO: Turn off the radio here
    mIsClosed = true;
    mThread.cancelAll();
}

Return<Result> Tuner::setConfiguration(const BandConfig& config) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;
    if (mClassId != Class::AM_FM) {
        ALOGE("Can't set AM/FM configuration on SAT/DT radio tuner");
        return Result::INVALID_STATE;
    }

    if (config.lowerLimit >= config.upperLimit) return Result::INVALID_ARGUMENTS;

    auto task = [this, config]() {
        ALOGI("Setting AM/FM config");
        lock_guard<mutex> lk(mMut);

        mAmfmConfig = move(config);
        mAmfmConfig.antennaConnected = true;
        mCurrentProgram = utils::make_selector(mAmfmConfig.type, mAmfmConfig.lowerLimit);

	pthread_mutex_lock(&radio_lock);

	// This is for BAND CHANGE. Tune to last frequency of band, if set, otherwise tune to default frequency.
/*        if (ptype >= 0){
            if (ptype == static_cast<uint32_t>(ProgramType::AM)){
                ALOGD("Tuning AM, sending command to radio...");
			tune_direct(pfreq, 0);
            } else {
                ALOGD("Tuning FM, sending command to radio...");
			tune_direct(pfreq/10, 1);
            }
        } else {
            if (utils::isFm(mAmfmConfig.type)) {
			tune_direct(9310, 1);
            } else {
			tune_direct(1010, 0);
            }
        }
*/
	pthread_mutex_unlock(&radio_lock);

	mIsAmfmConfigSet = true;
        mCallback->configChange(Result::OK, mAmfmConfig);

        mCurrentProgramInfo.base.channel = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
        mCurrentProgramInfo.selector.primaryId.type = 1;
        mCurrentProgramInfo.selector.primaryId.value = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
        mCurrentProgramInfo.base.tuned = 1;
        mCurrentProgramInfo.base.stereo = 1;
        mCurrentProgramInfo.base.digital = 0;
        mCurrentProgramInfo.base.signalStrength = 50;

	mIsTuneCompleted = true;
	mCallback1_1->tuneComplete_1_1(Result::OK, mCurrentProgramInfo.selector);
        mCallback1_1->currentProgramInfoChanged(mCurrentProgramInfo);

	if (runningRDS) termRDS = true;
	auto rtask = [this]() {
	        if (!runningRDS) runRDS();
	};
	mThread.schedule(rtask, 500ms);
    };
    mThread.schedule(task, gDefaultDelay.config);

    return Result::OK;
}

Return<void> Tuner::getConfiguration(getConfiguration_cb _hidl_cb) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);

    if (!mIsClosed && mIsAmfmConfigSet) {
        _hidl_cb(Result::OK, mAmfmConfig);
    } else {
        _hidl_cb(Result::NOT_INITIALIZED, {});
    }
    return {};
}

// makes ProgramInfo that points to no program
static ProgramInfo makeDummyProgramInfo(const ProgramSelector& selector) {
    ProgramInfo info11 = {};
    auto& info10 = info11.base;

    ALOGD("%s", __func__);

    utils::getLegacyChannel(selector, &info10.channel, &info10.subChannel);
    info10.tuned = 1;
    info10.stereo = 1;
    info10.digital = 0;
    info10.signalStrength = 50;
    info11.selector = selector;
    info11.flags |= ProgramInfoFlags::LIVE;

    return info11;
}

HalRevision Tuner::getHalRev() const {
    ALOGD("%s", __func__);
    if (mCallback1_1 != nullptr) {
        return HalRevision::V1_1;
    } else {
        return HalRevision::V1_0;
    }
}

void Tuner::reloadRDS(int changedType){
	switch(changedType){
		case 0:
			strncpy(pstext[1], pstext[0], 9);
			pschanged = false;
			break;
		case 1:
			strncpy(rtext[1], rtext[0], 65);
			rtchanged = false;
			break;
		case 2:
			strncpy(rds_genre[1], rds_genre[0], 32);
			rgchanged = false;
			break;
	}
	mCurrentProgramInfo.base.metadata = android::hardware::hidl_vec<android::hardware::broadcastradio::V1_0::MetaData>(4);
	mCurrentProgramInfo.base.metadata[0] = {
		android::hardware::broadcastradio::V1_0::MetadataType::TEXT,
		android::hardware::broadcastradio::V1_0::MetadataKey::RDS_PS,
		{},
		{},
		std::string(pstext[1]), // rds_ps type = char*
		{}
	};
	mCurrentProgramInfo.base.metadata[1] = {
		android::hardware::broadcastradio::V1_0::MetadataType::TEXT,
		android::hardware::broadcastradio::V1_0::MetadataKey::TITLE,//RDS_RT,
		{},
		{},
		std::string(rtext[1]),
		{}
	};
	mCurrentProgramInfo.base.metadata[2] = {
		android::hardware::broadcastradio::V1_0::MetadataType::TEXT,
		android::hardware::broadcastradio::V1_0::MetadataKey::GENRE,
		{},
		{},
		std::string(rds_genre[1]),
		{}
	};
        mCurrentProgramInfo.base.metadata[3] = {
                android::hardware::broadcastradio::V1_0::MetadataType::TEXT,
                android::hardware::broadcastradio::V1_0::MetadataKey::ARTIST,
                {},
                {},
                std::string(""),
                {}
        };
	mCallback1_1->currentProgramInfoChanged(mCurrentProgramInfo);
}

void Tuner::runRDS(){
	if (!mIsClosed && (Radio_GetCurrentBand() <= 2)){
		// Poll for RDS update...
		int r, i;
		uint16_t rds_stat, rds_a, rds_b, rds_c, rds_d, rds_err;
		int rds_offset;
		int rds_type = 0;
		char block_c[2];
		char block_d[2];

        	if (termRDS || clear_rds){
			memset(pstext[0], 0, 9);
			memset(pstext[1], 0, 9);
			psoffset = 0;
	                memset(rtext[0], 0, 65);
			memset(rtext[1], 0, 65);
			rtoffset = 0;
			memset(rds_genre[0], 0, 32);
			memset(rds_genre[1], 0, 32);
			rgoffset = 0;
        	        clear_rds = 0;
			reloadRDS(0);
			if (termRDS){
				termRDS = false;
				if (runningRDS){
					runningRDS = false;
				        return;
				}
			}
	        }
		runningRDS = true;

		pthread_mutex_lock(&radio_lock);
		r = devTEF668x_Radio_Get_RDS_Data(&rds_stat, &rds_a, &rds_b, &rds_c, &rds_d, &rds_err);
		pthread_mutex_unlock(&radio_lock);

		if (r && rds_b != 0x0 && (rds_stat & 0x8000) != 0x0){
			if ((rds_stat & 0x4000) != 0) ALOGE("RDS packet loss.\n");

			//ALOGD("Stat 0x%04X: A 0x%04X / B 0x%04X / C 0x%04X / D 0x%04X, ERR: 0x%04X\n", rds_stat, rds_a, rds_b, rds_c, rds_d, rds_err);

			/* Block A: PI code -- station unique identifier. Ignore for now.
			 * Block B: b15..b12 -- group type "0000" = station name, "0010" = radio text,
			 * 		 b11 -- 0 = type A, 1 = type B
			 * 		 b10 -- 1 = traffic program
			 * 	    b09..b05 -- program type code (i.e. news, weather, rock, etc.)
			 * 	    b04..b00 -- 
			 *
			 */

			if (((rds_b >> 8) & 0xf0) == 0x0){ // this is a PS message
				// If there is NOT an uncorrected error in block B or D (we don't care about A or C for PS)
				if ((rds_err & 0x0a00) == 0x0){ // other option is (rds_err & 0f00)
					rds_offset = (rds_b & 0x3) * 2;
					if (pschanged && rds_offset < psoffset) reloadRDS(0);
					psoffset = rds_offset;
					// Only process the update if data actually changes.
					if (pstext[0][rds_offset] != (rds_d >> 8) || pstext[0][rds_offset+1] != (rds_d & 0xff)){
						pstext[0][rds_offset] = rds_d >> 8;
						pstext[0][rds_offset+1] = rds_d & 0xff;
						pschanged = true;
					}
				}
			} else if (((rds_b >> 8) & 0xf0 )== 0x20){ // This is RADIO TEXT
				rds_type = (rds_b & 0x0800) >> 8;
				rds_offset = (rds_b & 0xf);
				if (rtchanged && rds_offset < rtoffset) reloadRDS(1);
				rtoffset = rds_offset;
				if (rds_type == 0){
					rds_offset *= 4; // radio text type A has 4 bytes per update in blocks C and D
					//ALOGD("RDS OFFSET: %d\n", rds_offset);
					// Radio text type A requires valid B, C, and D blocks.
					if ((rds_err & 0x0a00) == 0x0){ // other option is (rds_err & 0f00)
						if ((rds_b & 0x10) != toggleWipe) {
							memset(rtext[0], 0, 65);
							memset(rtext[1], 0, 65);
							toggleWipe = (rds_b & 0x10);
						}
						for (i = 0; i < rds_offset; i++) if (rtext[0][i] == 0) rtext[0][i] = 0x20;
						block_c[0] = rds_c >> 8;
						block_c[1] = rds_c & 0xff;
						block_d[0] = rds_d >> 8;
						block_d[1] = rds_d & 0xff;
						if (block_c[0] == 0x0d) block_c[0] = 0;
						if (block_c[1] == 0x0d) block_c[1] = 0;
						if (block_d[0] == 0x0d) block_d[0] = 0;
						if (block_d[1] == 0x0d) block_d[1] = 0;
						if (rtext[0][rds_offset] != block_c[0] || rtext[0][rds_offset+1] != block_c[1]
								|| rtext[0][rds_offset+2] != block_d[0] || rtext[0][rds_offset+3] != block_d[1]){

							rtext[0][rds_offset] = block_c[0];
							rtext[0][rds_offset+1] = block_c[1];
							rtext[0][rds_offset+2] = block_d[0];
							rtext[0][rds_offset+3] = block_d[1];
							rtchanged = true;
						}
					}
				} else {
					rds_offset *= 2; // radio text type B has 2 bytes per update in block D
					// Radio text type B requires valid B and D blocks.
					if (!((rds_err & 0x3000) == 0x3000 || (rds_err & 0x0300) == 0x0300)){
						if ((rds_b & 0x10) != toggleWipe) {
							memset(rtext[0], 0, 65);
							toggleWipe = (rds_b & 0x10);
						}
						for (i = 0; i < rds_offset; i++) if (rtext[0][i] == 0) rtext[0][i] = 0x20;
						block_d[0] = rds_d >> 8;
						block_d[1] = rds_d & 0xff;
						if (block_d[0] == 0x0d) block_d[0] = 0;
						if (block_d[1] == 0x0d) block_d[1] = 0;
						if (rtext[0][rds_offset] != block_d[0] || rtext[0][rds_offset+1] != block_d[1]){
							rtext[0][rds_offset] = block_d[0];
							rtext[0][rds_offset+1] = block_d[1];
							rtchanged = true;
						}
					}
				}
			}// else {
				//ALOGE("Unhandled GTYPE. RDS Block B: 0x%04X\n", rds_b);
			//}
		}

		// Schedule next RDS poll
		auto task = [this]() {
			runRDS();
		};
		mThread.schedule(task, ((rds_b == 0x0) ? gDefaultDelay.rds : 0ms));
	} else {
		memset(pstext[0], 0, 9);
		memset(pstext[1], 0, 9);
		psoffset = 0;
		memset(rtext[0], 0, 65);
		memset(rtext[1], 0, 65);
		rtoffset = 0;
		memset(rds_genre[0], 0, 32);
		memset(rds_genre[1], 0, 32);
		rgoffset = 0;
		clear_rds = 0;
		reloadRDS(0);
	}
}

void Tuner::tuneInternalLocked(const ProgramSelector& sel) {
    ALOGD("Tuning type: %d, value: %lu", sel.programType, sel.primaryId.value);

    if (runningRDS) termRDS = true;

    pthread_mutex_lock(&radio_lock);

    if (sel.primaryId.value < 5000){//programType == static_cast<uint32_t>(ProgramType::AM)){
	ALOGD("Tuning AM, sending command to radio...");
		tune_direct(sel.primaryId.value, 0);
    } else if (sel.programType == static_cast<uint32_t>(ProgramType::FM)){
	ALOGD("Tuning FM, sending command to radio...");
		tune_direct(sel.primaryId.value/10, 1);
    }

    pthread_mutex_unlock(&radio_lock);

    mIsTuneCompleted = true;

    mCurrentProgramInfo = makeDummyProgramInfo(sel);
    if (sel.primaryId.value < 5000) mCurrentProgramInfo.selector.programType = 0;

    if (mCallback1_1 == nullptr) {
        mCallback->tuneComplete(Result::OK, mCurrentProgramInfo.base);
    } else {
        mCallback1_1->tuneComplete_1_1(Result::OK, mCurrentProgramInfo.selector);
        mCallback1_1->currentProgramInfoChanged(mCurrentProgramInfo);
    }
    auto task = [this]() {
        if (!runningRDS) runRDS();
    };
    mThread.schedule(task, gDefaultDelay.tune);
}

// This is not actually scan. This is SEEK to next channel in whatever direction
Return<Result> Tuner::scan(Direction direction, bool skipSubChannel __unused) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;

    if (runningRDS) termRDS = true;
    mIsTuneCompleted = false;
    auto task = [this, direction]() {
        ALOGI("Performing seek %s", toString(direction).c_str());

        lock_guard<mutex> lk(mMut);

        int terminate = 0;
        int mode = 20;
        int stepcount = 0;
        int startfreq = Radio_GetCurrentFreq();

	pthread_mutex_lock(&radio_lock);

        while (!terminate){
	        switch(mode){
        	        case 20:/*Loop start*/
                	        stepcount++;
	                        /*increase or decreas one step*/
        	                Radio_ChangeFreqOneStep(direction == Direction::UP);

			        mCurrentProgramInfo.base.channel = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
			        mCurrentProgramInfo.selector.primaryId.type = 1;
			        mCurrentProgramInfo.selector.primaryId.value = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
			        mCurrentProgramInfo.base.tuned = 0;
			        mCurrentProgramInfo.base.stereo = 1;
			        mCurrentProgramInfo.base.digital = 0;
			        mCurrentProgramInfo.base.signalStrength = 50;

			        mCallback1_1->currentProgramInfoChanged(mCurrentProgramInfo);

                	        /*write "new programmble value" into PLL*/
                        	Radio_SetFreq(Radio_SEARCHMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());

	                        mode = 30;//set tunersubmode
        	                Radio_CheckStationInit();//set check SD and IF start step
                	        Radio_ClearCurrentStation();//clear station number
                        	break;

	                /*check Signel level and IF step after wait timer */
        	        case 30:
                	        /*station check*/
	                        usleep(20000);
        	                Radio_CheckStation();
                	        if(Radio_CheckStationStatus() >= NO_STATION)
                        	        mode = 40;
	                        break;

        	        case 40:
                        	if(Radio_CheckStationStatus()== NO_STATION)  {/*no station*/
                                	if(startfreq==Radio_GetCurrentFreq())
                                        	mode = 50;
	                                else
        	                                mode = 20;
                	        } else if(Radio_CheckStationStatus()== PRESENT_STATION){
                                	mode = 50;
	                        }
        	                break;

        	        case 50:/*stop Seek Tuning*/
	                        /*Tuner finish current work mode*/
                        	Radio_SetFreq(Radio_PRESETMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());

                                mCurrentProgramInfo.base.channel = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
                                mCurrentProgramInfo.selector.primaryId.type = 1;
                                mCurrentProgramInfo.selector.primaryId.value = Radio_GetCurrentFreq() * ((Radio_GetCurrentBand() <= 2) ? 10 : 1);
                                mCurrentProgramInfo.base.tuned = 1;
                                mCurrentProgramInfo.base.stereo = 1;
                                mCurrentProgramInfo.base.digital = 0;
                                mCurrentProgramInfo.base.signalStrength = 50;

				mIsTuneCompleted = true;
				mCallback1_1->tuneComplete_1_1(Result::OK, mCurrentProgramInfo.selector);
                                mCallback1_1->currentProgramInfoChanged(mCurrentProgramInfo);
				auto rtask = [this]() {
		                        if (!runningRDS) runRDS();
                		};
		                mThread.schedule(rtask, 500ms);

                	        terminate = 1;
	        }
        }
	pthread_mutex_unlock(&radio_lock);

    };
    mThread.schedule(task, gDefaultDelay.scan);

    return Result::OK;
}

Return<Result> Tuner::step(Direction direction, bool skipSubChannel) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;

    ALOGW_IF(!skipSubChannel, "can't step to next frequency without ignoring subChannel");

    if (!utils::isAmFm(utils::getType(mCurrentProgram))) {
        ALOGE("Can't step in anything else than AM/FM");
        return Result::NOT_INITIALIZED;
    }

    if (!mIsAmfmConfigSet) {
        ALOGW("AM/FM config not set");
        return Result::INVALID_STATE;
    }
    mIsTuneCompleted = false;

    auto task = [this, direction]() {
        ALOGI("Performing step %s", toString(direction).c_str());

        lock_guard<mutex> lk(mMut);

        auto current = utils::getId(mCurrentProgram, IdentifierType::AMFM_FREQUENCY, 0);

        if (direction == Direction::UP) {
            current += mAmfmConfig.spacings[0];
        } else {
            current -= mAmfmConfig.spacings[0];
        }

        if (current > mAmfmConfig.upperLimit) current = mAmfmConfig.lowerLimit;
        if (current < mAmfmConfig.lowerLimit) current = mAmfmConfig.upperLimit;

        tuneInternalLocked(utils::make_selector(mAmfmConfig.type, current));
    };
    mThread.schedule(task, gDefaultDelay.step);

    return Result::OK;
}

Return<Result> Tuner::tune(uint32_t channel, uint32_t subChannel) {
    ALOGD("%s(%d, %d)", __func__, channel, subChannel);
    Band band;
    {
        lock_guard<mutex> lk(mMut);
        band = mAmfmConfig.type;
    }
    return tuneByProgramSelector(utils::make_selector(band, channel, subChannel));
}

Return<Result> Tuner::tuneByProgramSelector(const ProgramSelector& sel) {
    ALOGD("%s(%s)", __func__, toString(sel).c_str());
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;

    pfreq = sel.primaryId.value;
    ptype = sel.programType;

    // checking if ProgramSelector is valid
    auto programType = utils::getType(sel);
    if (utils::isAmFm(programType)) {
        if (!mIsAmfmConfigSet) {
            ALOGW("AM/FM config not set");
            return Result::INVALID_STATE;
        }

        auto freq = utils::getId(sel, IdentifierType::AMFM_FREQUENCY);
        if (freq < 100){//mAmfmConfig.lowerLimit || freq > mAmfmConfig.upperLimit) {
	    ALOGD("freq: %lu, lowerLimit: %d, upperLimit: %d", freq, mAmfmConfig.lowerLimit, mAmfmConfig.upperLimit);
            return Result::INVALID_ARGUMENTS;
        }
    } else if (programType == ProgramType::DAB) {
	    ALOGD("Attempting to tune DAB, invalid");
        if (!utils::hasId(sel, IdentifierType::DAB_SIDECC)) return Result::INVALID_ARGUMENTS;
    } else if (programType == ProgramType::DRMO) {
	    ALOGD("Attempting to tune DRMO, invalid");
        if (!utils::hasId(sel, IdentifierType::DRMO_SERVICE_ID)) return Result::INVALID_ARGUMENTS;
    } else if (programType == ProgramType::SXM) {
	    ALOGD("Attempting to tune SXM, invalid");
        if (!utils::hasId(sel, IdentifierType::SXM_SERVICE_ID)) return Result::INVALID_ARGUMENTS;
    } else {
	    ALOGD("Attempting to tune <<unknown>>, invalid");
        return Result::INVALID_ARGUMENTS;
    }

    mIsTuneCompleted = false;
    auto task = [this, sel]() {
        lock_guard<mutex> lk(mMut);
        tuneInternalLocked(sel);
    };
    mThread.schedule(task, gDefaultDelay.tune);

    return Result::OK;
}

Return<Result> Tuner::cancel() {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;

    mThread.cancelAll();
    return Result::OK;
}

Return<Result> Tuner::cancelAnnouncement() {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;

    return Result::OK;
}

Return<void> Tuner::getProgramInformation(getProgramInformation_cb _hidl_cb) {
    ALOGD("%s", __func__);
    return getProgramInformation_1_1([&](Result result, const ProgramInfo& info) {
        _hidl_cb(result, info.base);
    });
}

Return<void> Tuner::getProgramInformation_1_1(getProgramInformation_1_1_cb _hidl_cb) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);

    if (mIsClosed) {
        _hidl_cb(Result::NOT_INITIALIZED, {});
    } else if (mIsTuneCompleted) {
        _hidl_cb(Result::OK, mCurrentProgramInfo);
    } else {
        _hidl_cb(Result::NOT_INITIALIZED, makeDummyProgramInfo(mCurrentProgram));
    }
    return {};
}

Return<ProgramListResult> Tuner::startBackgroundScan() {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return ProgramListResult::NOT_INITIALIZED;

    return ProgramListResult::UNAVAILABLE;
}

Return<void> Tuner::getProgramList(const hidl_vec<VendorKeyValue>& vendorFilter,
                                   getProgramList_cb _hidl_cb) {
    ALOGD("%s(%s)", __func__, toString(vendorFilter).substr(0, 100).c_str());
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) {
        _hidl_cb(ProgramListResult::NOT_INITIALIZED, {});
        return {};
    }

    ALOGD("returning a list of 0 programs");
    _hidl_cb(ProgramListResult::OK, {});
    return {};
}

Return<Result> Tuner::setAnalogForced(bool isForced) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);
    if (mIsClosed) return Result::NOT_INITIALIZED;
    mIsAnalogForced = isForced;
    return Result::OK;
}

Return<void> Tuner::isAnalogForced(isAnalogForced_cb _hidl_cb) {
    ALOGD("%s", __func__);
    lock_guard<mutex> lk(mMut);

    if (mIsClosed) {
        _hidl_cb(Result::NOT_INITIALIZED, false);
    } else {
        _hidl_cb(Result::OK, mIsAnalogForced);
    }
    return {};
}

}  // namespace implementation
}  // namespace V1_1
}  // namespace broadcastradio
}  // namespace hardware
}  // namespace android
