#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <android/log.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <libusb/libusb.h>

#include <sys/system_properties.h>
#include <cutils/properties.h>

static const int MAX_INTERRUPT_OUT_TRANSFER_SIZE = 0x2;

static const int VENDOR_ID = 0x16c0;
static const int PRODUCT_ID = 0x05dc;
static const int INTERFACE_NUMBER = 4; // 0-based interface index.

static const int INTERRUPT_OUT_ENDPOINT = 0x01+INTERFACE_NUMBER;
static const int TIMEOUT_MS = 5000;

static struct libusb_device_handle *devh = NULL;

void writehid(uint8_t value){

	int bytes_transferred;
	int result = 0;

	uint8_t data_out[MAX_INTERRUPT_OUT_TRANSFER_SIZE];

	// Store data in a buffer for sending.
	data_out[0] = 0x01;
	data_out[1] = value;

	// Write data to the device.
	result = libusb_interrupt_transfer(
			devh,
			INTERRUPT_OUT_ENDPOINT,
			data_out,
			MAX_INTERRUPT_OUT_TRANSFER_SIZE,
			&bytes_transferred,
			TIMEOUT_MS);

	if (result < 0){
		libusb_release_interface(devh, 0);
		libusb_close(devh);
		libusb_exit(NULL);
		devh = NULL;
	}
}

void openhid(){
	int result = libusb_init(NULL);
	if (result >= 0){
		devh = libusb_open_device_with_vid_pid(NULL, VENDOR_ID, PRODUCT_ID);

		if (devh != NULL){
			// The HID has been detected.
			// Detach the hidusb driver from the HID to enable using libusb.
			libusb_detach_kernel_driver(devh, INTERFACE_NUMBER);
			result = libusb_claim_interface(devh, INTERFACE_NUMBER);
			if (result < 0){
				libusb_close(devh);
				libusb_exit(NULL);
				devh = NULL;
			}
		}
	}
}

int main(){

        int lasttemp = 0, curtemp;
        unsigned char lastpwm = 0x00;
        unsigned char curpwm = 0x00;
        char buff[10];

        int tempfd = open("/sys/devices/virtual/thermal/thermal_zone0/temp", O_RDONLY);
        if (tempfd <= 0) return 0;
        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Entered thermal monitoring loop");

        while (1){
                lseek(tempfd, 0, SEEK_SET);
                read(tempfd, buff, 6);
                curtemp = atoi(buff);
                __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Read temperature: %d", curtemp);
                if (curtemp > lasttemp){
                        // temperature is increasing
                        if (curtemp >= 65000) curpwm = 0xff; // 100%
                        else if (curtemp >= 60000) curpwm = 0xe5; // 90%
                        else if (curtemp >= 55000) curpwm = 0xcc; // 80%
                        else if (curtemp >= 50000) curpwm = 0xb2; // 70%
                        else if (curtemp >= 45000) curpwm = 0x99; // 60%
                        else if (curtemp >= 40000) curpwm = 0x7f; // 50%
                } else {
                        // temperature is decreasing
                        if (curtemp < 35000) curpwm = 0x00; // 0%
                        else if (curtemp < 40000) curpwm = 0x7f; // 50%
                        else if (curtemp < 45000) curpwm = 0x99; // 60%
                        else if (curtemp < 50000) curpwm = 0xb2; // 70%
                        else if (curtemp < 55000) curpwm = 0xcc; // 80%
                        else if (curtemp < 60000) curpwm = 0xe5; // 90%
                }
                if (curpwm != lastpwm){
                        __android_log_print(ANDROID_LOG_DEBUG, "thermalfand", "Sending fan update: %02X", curpwm);
                        if (devh == NULL) openhid();
                        if (devh != NULL) writehid(curpwm);
                        lastpwm = curpwm;
                        lasttemp = curtemp;
                }
                sleep(1);
        }
}

