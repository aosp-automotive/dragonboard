package tk.rabidbeaver.carsettings;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.content.SharedPreferences;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;

import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;

import java.util.HashSet;
import java.util.Set;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class CarSettings extends Activity {
	Spinner spin;
	TextView adcout1, adcout2;
	Thread th;
	boolean adcrunner;
	boolean success;
	byte[] swival;
	byte swi1hi, swi1lo, swi2hi, swi2lo;

	static final int usbVID = 0x16c0;
	static final int usbPID = 0x05dc;
	static final int usbInt = 2;
	UsbDevice usbDevice = null;
	UsbInterface usbInterface = null;
	UsbEndpoint epIn = null;
	UsbEndpoint epOut = null;
	UsbDeviceConnection usbDeviceConnection;
	UsbRequest usbReceiver;
	ByteBuffer usbReceiverBuffer;
	UsbRequest usbSender;
	ByteBuffer usbSenderBuffer;
	Object usbMutex = new Object();

	private static final String ACTION_USB_PERMISSION =
			"com.android.example.USB_PERMISSION";
	PendingIntent mPermissionIntent;

	private boolean setContainsString(Set<String> set, String string){
		Object[] setarr = set.toArray();
		for (int i=0; i<set.size(); i++){
			if (((String)setarr[i]).contentEquals(string)) return true;
		}
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configure_swi);

		mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		registerReceiver(mUsbReceiver, filter);

		SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
		Set<String> selectedDevices = prefs.getStringSet("devices", new HashSet<String>());

		boolean autoconnect = prefs.getBoolean("autoconnect", false);
		boolean autohotspot = prefs.getBoolean("autohotspot", false);

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		PairedDev[] pda = new PairedDev[pairedDevices.size()];
		boolean[] selected = new boolean[pairedDevices.size()];

		int i = 0;
		for (BluetoothDevice device : pairedDevices) {
			Log.d("BluetoothTethering",device.getName()+", "+device.getAddress());
			if (selectedDevices.size() > 0 && setContainsString(selectedDevices, device.getAddress())) selected[i] = true;
			pda[i] = new PairedDev(device);
			i++;
		}

		final MultiSpinner spinner = (MultiSpinner) findViewById(R.id.devspin);
		final ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, pda);
		spinner.setAdapter(spinnerArrayAdapter, false, null);

		spinner.setSelected(selected);

		Switch panswitch = findViewById(R.id.autopan);
		panswitch.setChecked(autoconnect);
		panswitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
				editor.putBoolean("autoconnect", isChecked);
				Set<String> selectedItems = new HashSet<>();
				for (int i=0; i<spinnerArrayAdapter.getCount(); i++){
					if (spinner.getSelected()[i]) selectedItems.add(((PairedDev)spinnerArrayAdapter.getItem(i)).getDev());
				}
				editor.putStringSet("devices", selectedItems);
				editor.apply();
			}
		});

		Switch hotspotswitch = findViewById(R.id.onhotspot);
		hotspotswitch.setChecked(autohotspot);
		hotspotswitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
				editor.putBoolean("autohotspot", isChecked);
				editor.apply();
			}
		});

		adcout1 = findViewById(R.id.adcout1);
		adcout2 = findViewById(R.id.adcout2);

		Button wipebtn = findViewById(R.id.wipebtn);
		wipebtn.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View btn){
				send(new byte[] {0x01, 0x05});
			}
		});

		Button rebootbtn = findViewById(R.id.rebootbtn);
		rebootbtn.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View btn){
				send(new byte[] {0x01, 0x09});
				close();
				try {
					Thread.sleep(1000);
				} catch (Exception e){
					e.printStackTrace();
				}
				getDevice();
				open();
			}
		});

		Button addswi1btn = findViewById(R.id.addswi1btn);
		addswi1btn.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View btn){
				byte bhi = ((CodeValue) spin.getSelectedItem()).codehi;
				byte blo = ((CodeValue) spin.getSelectedItem()).codelo;
				send(new byte[] {0x01, 0x07, 0x00, swi1hi, swi1lo, bhi, blo});
			}
		});

		Button addswi2btn = findViewById(R.id.addswi2btn);
		addswi2btn.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View btn){
				byte bhi = ((CodeValue) spin.getSelectedItem()).codehi;
				byte blo = ((CodeValue) spin.getSelectedItem()).codelo;
				send(new byte[] {0x01, 0x07, 0x01, swi2hi, swi2lo, bhi, blo});
			}
		});

		Button upgradeBtn = findViewById(R.id.progbtn);
		upgradeBtn.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View btn){
				TextView upgmsg = findViewById(R.id.upgmsg);
				upgmsg.setText("Firmware upgrade in progress. Please wait.");

				Intent intent = new Intent()
						.setType("*/*")
						.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent, "Select firmware"), 123);
			}
		});

		spin = findViewById(R.id.hidcodes);
		List<CodeValue> mCodeValues = genlist();
		ArrayAdapter codeAdapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, mCodeValues);
		spin.setAdapter(codeAdapter);

		Log.d("CarSettings", "Starting CarLocationService");
		Intent startServiceIntent = new Intent(this, CarLocationService.class);
		startService(startServiceIntent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 123 && resultCode == RESULT_OK) {
			Uri selectedfile = data.getData();
			if (!selectedfile.toString().startsWith("content://com.android.providers.downloads.documents/document/raw%3A")){
				TextView upgmsg = findViewById(R.id.upgmsg);
				upgmsg.setText("Invalid file");

				return;
			}
			final String fwpath = selectedfile.toString().substring(67).replace("%2F", "/");
			Log.d("SWIFIRMWARE", fwpath);

			new Thread(new Runnable(){
				@Override
				public void run(){
					send(new byte[] {0x01, 0x0a});
					close();

					// If the machine has rebooted after the mcu was wiped, then gpsd could interfere with bossac's access to the
					// serial port. Stop gnss-gpsd before attempting to write the MCU.
					String[] cmd1 = { "sh", "-c", "/system/bin/stop gnss-gpsd" };
					try {
						Process p = Runtime.getRuntime().exec(cmd1);
						p.waitFor();
					} catch (Exception e){
						e.printStackTrace();
					}

					success = false;
					for (int i = 0; i < 5 && !success; i++){
						String[] cmdline = { "sh", "-c", "/vendor/bin/logwrapper /vendor/bin/bossac -i -d --port=/dev/ttyACM"
								+ Integer.toString(i) + " -U -i --offset=0x2000 -w -v " + fwpath + " -R" };

						try {
							if (i == 0) Thread.sleep(1000);
							Process p = Runtime.getRuntime().exec(cmdline);
							p.waitFor();
							if (p.exitValue() == 0) success = true;
						} catch (Exception e){
							e.printStackTrace();
						}
					}

					runOnUiThread(new Runnable(){
						@Override
						public void run(){
							TextView upgmsg = findViewById(R.id.upgmsg);
							if (success) upgmsg.setText("Firmware upgrade SUCCESS.");
							else upgmsg.setText("Firmware upgrade FAILURE.");
						}
					});

					getDevice();
					open();
				}
			}).start();
		}
	}

	@Override
	public void onPause(){
		super.onPause();
		close();
	}

	public void getDevice(){
		usbDevice = null;
		UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
		HashMap<String, UsbDevice> deviceList = manager.getDeviceList();

		for (UsbDevice device : deviceList.values()) {
			if (device.getVendorId() == usbVID && device.getProductId() == usbPID) {
				usbDevice = device;
				return;
			}
		}
	}

	public void open(){
		if (usbDevice == null) return;

		usbInterface = null;
		epOut = null;
		epIn = null;

		UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
		if (!manager.hasPermission(usbDevice)){
			manager.requestPermission(usbDevice, mPermissionIntent);
			return;
		}

		UsbInterface usbif = usbDevice.getInterface(usbInt);
		UsbEndpoint tOut = null;
		UsbEndpoint tIn = null;

		for (int j = 0; j < usbif.getEndpointCount(); j++){
			if (usbif.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_INT){
				if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_OUT){
					tOut = usbif.getEndpoint(j);
				} else if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_IN){
					tIn = usbif.getEndpoint(j);
				}
			}
		}

		if (tOut != null && tIn != null){
			usbInterface = usbif;
			epOut = tOut;
			epIn = tIn;
		}

		if (usbInterface == null || epOut == null || epIn == null) return;
		if ((usbDeviceConnection = manager.openDevice(usbDevice)) == null) return;

		if (usbDeviceConnection.claimInterface(usbInterface, true)){
			Log.d("CarSettings", "USB interface claimed");
			usbReceiver = new UsbRequest();
			usbReceiver.initialize(usbDeviceConnection, epIn);
			usbReceiverBuffer = ByteBuffer.allocate(16);
			usbSender = new UsbRequest();
			usbSender.initialize(usbDeviceConnection, epOut);
			usbSenderBuffer = ByteBuffer.allocate(16);
		} else {
			epIn = null;
			epOut = null;
			return;
		}

		send(new byte[] {0x01, 0x01});

		adcrunner = true;
		th = new Thread(new Runnable(){
			@Override
			public void run(){	
				while (adcrunner){
					swival = receive();
					if (swival == null) continue;
					swi1hi = swival[1]; swi1lo = swival[2];
					swi2hi = swival[3]; swi2lo = swival[4];

					runOnUiThread(new Runnable(){
						@Override
						public void run(){
							adcout1.setText(String.format("0x%02X%02X", swival[1], swival[2]));
							adcout2.setText(String.format("0x%02X%02X", swival[3], swival[4]));
						}
					});
				}
			}
		});
		th.start();
	}

	public void close(){
		adcrunner = false;
		try {
			th.interrupt();
		} catch (Exception e){e.printStackTrace();}
		send(new byte[] {0x01, 0x02});

		if(usbDeviceConnection != null){
			if(usbInterface != null){
				usbDeviceConnection.releaseInterface(usbInterface);
				usbInterface = null;
			}
			usbDeviceConnection.close();
			usbDeviceConnection = null;
		}

		usbDevice = null;
		usbInterface = null;
		epIn = null;
		epOut = null;
	}

	public void send(byte[] buff){
		Log.d("Car","Sending report");
		if (epOut == null){
			Log.d("Car", "epOut is null");
			return;
		}
		usbSenderBuffer.clear();
		usbSenderBuffer.put(buff);
		try {
			synchronized (usbMutex) {
				/*  The non-deprecated version of queue, without the length parameter, is defective.
				 *  instead of sending the data in the buffer, it sends the empty space that is
				 *  available after the valid data that should be sent. Consequently, we have to
				 *  continue using the so-called "deprecated" version of this function.
				 */
				usbSender.queue(usbSenderBuffer, buff.length);
				usbDeviceConnection.requestWait();
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public byte[] receive(){
		if (epIn == null){
			Log.d("Car", "epIn is null");
			return null;
		}
		usbReceiverBuffer.clear();
		try {
			synchronized (usbMutex) {
				usbReceiver.queue(usbReceiverBuffer);
				usbDeviceConnection.requestWait();
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		byte[] retData = usbReceiverBuffer.array();
		if (retData.length == 0) return null;
		return retData;
	}

	@Override
	public void onResume(){
		super.onResume();
		getDevice();
		open();
	}

	public void onSetTouchscreen(View view) {
		CheckBox enable = findViewById(R.id.gt911enable);
		CheckBox i2c0x14 = findViewById(R.id.gt9110x14);
		CheckBox chipreset = findViewById(R.id.gt911reset);
		CheckBox swap = findViewById(R.id.gt911swapxy);
		CheckBox invx = findViewById(R.id.gt911invx);
		CheckBox invy = findViewById(R.id.gt911invy);
		EditText padleft = findViewById(R.id.padleft);
		EditText padright = findViewById(R.id.padright);
		EditText padtop = findViewById(R.id.padtop);
		EditText padbottom = findViewById(R.id.padbottom);
		int left = Integer.parseInt(padleft.getText().toString());
		if (left > 1000){
			left = 1000;
			padleft.setText("1000");
		}
		int right = Integer.parseInt(padright.getText().toString());
		if (right > 1000){
			right = 1000;
			padright.setText("1000");
		}
		int top = Integer.parseInt(padtop.getText().toString());
		if (top > 1000){
			top = 1000;
			padtop.setText("1000");
		}
		int bottom = Integer.parseInt(padbottom.getText().toString());
		if (bottom > 1000){
			bottom = 1000;
			padbottom.setText("1000");
		}

		byte[] buffer = new byte[16];
		buffer[0] = 0x01;
		buffer[1] = 0x03;
		buffer[2] = enable.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[3] = i2c0x14.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[4] = chipreset.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[5] = invx.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[6] = invy.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[7] = swap.isChecked() ? (byte)0x01 : (byte)0x00;
		buffer[8] = (byte)(left >> 8 & 0xff);
		buffer[9] = (byte)(left & 0xff);
		buffer[10] = (byte)(right >> 8 & 0xff);
		buffer[11] = (byte)(right & 0xff);
		buffer[12] = (byte)(top >> 8 & 0xff);
		buffer[13] = (byte)(top & 0xff);
		buffer[14] = (byte)(bottom >> 8 & 0xff);
		buffer[15] = (byte)(bottom & 0xff);

		send(buffer);
	}

	public void onSetMinBacklight(View view) {
		EditText brightmin = findViewById(R.id.brightmin);
		int minbright = Integer.parseInt(brightmin.getText().toString());
		if (minbright > 0xff){
			minbright = 0xff;
			brightmin.setText("255");
		}
		send(new byte[] {0x01, 0x04, (byte)minbright});
	}

	private class PairedDev {
		String dev;
		String name;
		PairedDev(BluetoothDevice btd){
			name = btd.getName();
			dev = btd.getAddress();
		}
		public String toString(){return name;}
		String getDev(){return dev;}
	}

	private List<CodeValue> genlist(){
		List<CodeValue> mCodeValues = new ArrayList<CodeValue>();
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x30, "KEY_POWER"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x31, "KEY_RESTART"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x32, "KEY_SLEEP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x34, "KEY_SLEEP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x35, "KEY_KBDILLUMTOGGLE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x36, "BTN_MISC"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x40, "KEY_MENU"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x41, "KEY_SELECT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x42, "KEY_UP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x43, "KEY_DOWN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x44, "KEY_LEFT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x45, "KEY_RIGHT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x46, "KEY_ESC"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x47, "KEY_KPPLUS"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x48, "KEY_KPMINUS"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x60, "KEY_INFO"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x61, "KEY_SUBTITLE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x63, "KEY_VCR"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x65, "KEY_CAMERA"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x69, "KEY_RED"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x6a, "KEY_GREEN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x6b, "KEY_BLUE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x6c, "KEY_YELLOW"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x6d, "KEY_ASPECT_RATIO"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x6f, "KEY_BRIGHTNESSUP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x70, "KEY_BRIGHTNESSDOWN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x72, "KEY_BRIGHTNESS_TOGGLE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x73, "KEY_BRIGHTNESS_MIN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x74, "KEY_BRIGHTNESS_MAX"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x75, "KEY_BRIGHTNESS_AUTO"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x79, "KEY_KBDILLUMUP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x7a, "KEY_KBDILLUMDOWN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x7c, "KEY_KBDILLUMTOGGLE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x82, "KEY_VIDEO_NEXT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x83, "KEY_LAST"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x84, "KEY_ENTER"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x88, "KEY_PC"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x89, "KEY_TV"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8a, "KEY_WWW"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8b, "KEY_DVD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8c, "KEY_PHONE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8d, "KEY_PROGRAM"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8e, "KEY_VIDEOPHONE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x8f, "KEY_GAMES"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x90, "KEY_MEMO"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x91, "KEY_CD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x92, "KEY_VCR"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x93, "KEY_TUNER"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x94, "KEY_EXIT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x95, "KEY_HELP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x96, "KEY_TAPE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x97, "KEY_TV2"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x98, "KEY_SAT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x9a, "KEY_PVR"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x9c, "KEY_CHANNELUP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0x9d, "KEY_CHANNELDOWN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xa0, "KEY_VCR2"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb0, "KEY_PLAY"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb1, "KEY_PAUSE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb2, "KEY_RECORD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb3, "KEY_FASTFORWARD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb4, "KEY_REWIND"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb5, "KEY_NEXTSONG"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb6, "KEY_PREVIOUSSONG"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb7, "KEY_STOPCD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb8, "KEY_EJECTCD"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xbc, "KEY_MEDIA_REPEAT"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xb9, "KEY_SHUFFLE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xbf, "KEY_SLOW"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xcd, "KEY_PLAYPAUSE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xcf, "KEY_VOICECOMMAND"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xe0, "ABS_VOLUME"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xe2, "KEY_MUTE"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xe5, "KEY_BASSBOOST"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xe9, "KEY_VOLUMEUP"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xea, "KEY_VOLUMEDOWN"));
		mCodeValues.add(new CodeValue((byte)0x00, (byte)0xf5, "KEY_SLOW"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x81, "KEY_BUTTONCONFIG"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x82, "KEY_BOOKMARKS"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x83, "KEY_CONFIG"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x84, "KEY_WORDPROCESSOR"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x85, "KEY_EDITOR"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x86, "KEY_SPREADSHEET"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x87, "KEY_GRAPHICSEDITOR"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x88, "KEY_PRESENTATION"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x89, "KEY_DATABASE"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8a, "KEY_MAIL"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8b, "KEY_NEWS"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8c, "KEY_VOICEMAIL"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8d, "KEY_ADDRESSBOOK"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8e, "KEY_CALENDAR"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x8f, "KEY_TASKMANAGER"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x90, "KEY_JOURNAL"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x91, "KEY_FINANCE"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x92, "KEY_CALC"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x93, "KEY_PLAYER"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x94, "KEY_FILE"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x96, "KEY_WWW"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x99, "KEY_CHAT"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x9c, "KEY_LOGOFF"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x9e, "KEY_COFFEE"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0x9f, "KEY_CONTROLPANEL"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xa2, "KEY_APPSELECT"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xa3, "KEY_NEXT"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xa4, "KEY_PREVIOUS"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xa6, "KEY_HELP"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xa7, "KEY_DOCUMENTS"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xab, "KEY_SPELLCHECK"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xae, "KEY_KEYBOARD"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xb1, "KEY_SCREENSAVER"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xb4, "KEY_FILE"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xb6, "KEY_IMAGES"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xb7, "KEY_AUDIO"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xb8, "KEY_VIDEO"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xbc, "KEY_MESSENGER"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xbd, "KEY_INFO"));
		mCodeValues.add(new CodeValue((byte)0x01, (byte)0xcb, "KEY_ASSISTANT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x01, "KEY_NEW"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x02, "KEY_OPEN"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x03, "KEY_CLOSE"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x04, "KEY_EXIT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x07, "KEY_SAVE"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x08, "KEY_PRINT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x09, "KEY_PROPS"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x1a, "KEY_UNDO"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x1b, "KEY_COPY"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x1c, "KEY_CUT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x1d, "KEY_PASTE"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x1f, "KEY_FIND"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x21, "KEY_SEARCH"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x22, "KEY_GOTO"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x23, "KEY_HOMEPAGE"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x24, "KEY_BACK"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x25, "KEY_FORWARD"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x26, "KEY_STOP"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x27, "KEY_REFRESH"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x2a, "KEY_BOOKMARKS"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x2d, "KEY_ZOOMIN"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x2e, "KEY_ZOOMOUT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x2f, "KEY_ZOOMRESET"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x32, "KEY_FULL_SCREEN"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x33, "KEY_SCROLLUP"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x34, "KEY_SCROLLDOWN"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x38, "KEY_AC_PAN"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x3d, "KEY_EDIT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x5f, "KEY_CANCEL"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x69, "KEY_INSERT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x6a, "KEY_DELETE"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x79, "KEY_REDO"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x89, "KEY_REPLY"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x8b, "KEY_FORWARDMAIL"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x8c, "KEY_SEND"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x9d, "KEY_KBD_LAYOUT_NEXT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xc7, "KEY_KBDINPUTASSIST_PREV"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xc8, "KEY_KBDINPUTASSIST_NEXT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xc9, "KEY_KBDINPUTASSIST_PREVGROUP"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xca, "KEY_KBDINPUTASSIST_NEXTGROUP"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xcb, "KEY_KBDINPUTASSIST_ACCEPT"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0xcc, "KEY_KBDINPUTASSIST_CANCEL"));
		mCodeValues.add(new CodeValue((byte)0x02, (byte)0x9f, "KEY_SCALE"));
		mCodeValues.add(new CodeValue((byte)0xff, (byte)0xff, "__RESET SBC__"));
		return mCodeValues;
	}

	private class CodeValue {
	byte codehi;
	byte codelo;
		String name;

		CodeValue(byte codehi, byte codelo, String mName){
			this.codehi = codehi;
			this.codelo = codelo;
			name = "0x" + String.format("%02X ", codehi) + String.format("%02X ", codelo) + ": " + mName;
		}

		@Override
		public String toString() {
			return this.name;
		}
	}

	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
					if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						if(device != null){
							usbDevice = device;
						}
					}
				}
			}
		}
	};
}

